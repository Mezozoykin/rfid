package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 04.01.2017.
 */

public class DeleteBatteryRequest extends BaseRequest<DeleteBatteryRequestParams> {

    public DeleteBatteryRequest(int id) {
        super(new DeleteBatteryRequestParams(id)) ;
    }

    @Override
    public String getCommand() {
        return "battery_delete";
    }
}



    class DeleteBatteryRequestParams extends BaseRequestParams {
        @SerializedName("id")
        private int id;

        public DeleteBatteryRequestParams(int id) {
            this.id = id;
        }
    }

