package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;


public class AddSiteRequest extends BaseRequest<AddSiteParams> {

    public AddSiteRequest(String title,String comment, String lat, String lon) {
        super(new AddSiteParams(title, comment, lat, lon));
    }

    public AddSiteRequest(int id, String title, String comment, String lat, String lon) {
        super(new AddSiteParams(id, title, comment, lat, lon));
    }

    @Override
    public String getCommand() {
        return "site";
    }
}
       class AddSiteParams extends BaseRequestParams {

           @SerializedName("id")
           int id;

           @SerializedName("title")
           String title;

           @SerializedName("comment")
           String comment;

           @SerializedName("lat")
           String lat;

           @SerializedName("lon")
           String lon;

           public AddSiteParams(String title, String comment, String lat, String lon) {
               this.title = title;
               this.comment = comment;
               this.lat = lat;
               this.lon = lon;

           }

           public AddSiteParams(int id, String title, String comment, String lat, String lon) {
               this.id = id;
               this.title = title;
               this.comment = comment;
               this.lat = lat;
               this.lon = lon;
           }
       }

