package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.sites;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import biz.ideus.ideuslib.mvvm_lifecycle.AbstractViewModel;
import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.add_site_screen.AddSiteFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;


public class SiteFragmentVM extends AbstractViewModel<IView> {

    private Context context;

    @Override
    public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
        super.onCreate(arguments, savedInstanceState);
    }

    @Override
    public void onBindView(@NonNull IView view) {
        super.onBindView(view);
        context = view.getViewModelBindingConfig().getContext();

    }

    public void clickAddSite(View view){
        ((MainActivity)context).addFragment(new AddSiteFragment(), true);
    }
}
