package ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model;

import android.databinding.Observable;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by user on 29.12.2016.
 */

@Entity
public interface Battery extends Observable, Parcelable, Persistable {
    @Key
    String getId();
    void setId(String id);



    String getRoomId();
    void setRoomId(String roomId);

    String getAccountId();
    void setAccountId(String accountId);

    String getRfidNumber();
    void setRfidNumber(String rfidNumber);


    String getManufacturer();
    void setManufacturer(String manufacturer);

    String getType();
    void setType(String type);

    String getSerialNumber();
    void setSerialNumber(String serialNumber);

    String getShippingDate();
    void setShippingDate(String shippingDate);

    String getSoldVendor();
    void setSoldVendor(String soldVendor);

    String getChargeDate();
    void setChargeDate(String chargeDate);

    String getModificationDate();
    void setModificationDate(String modificationDate);

    String getModificationTime();
    void setModificationTime(String modificationTime);


    String getPhoto();
    void setPhoto(String photo);
}


