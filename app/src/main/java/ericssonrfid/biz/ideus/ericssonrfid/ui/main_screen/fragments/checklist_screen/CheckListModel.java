package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.checklist_screen;


public class CheckListModel {
    private String title;
    private boolean isChecked;

    public CheckListModel(){}
    public CheckListModel(String title, boolean isChecked){
        this.title = title;
        this.isChecked = isChecked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

}
