package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.summary;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import biz.ideus.ideuslib.mvvm_lifecycle.AbstractViewModel;
import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemSummaryFourBinding;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemSummaryOneBinding;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemSummaryThreeBinding;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemSummaryTwoBinding;
import ericssonrfid.biz.ideus.ericssonrfid.models.data_base.BatterySummaryParamsEntity;
import ericssonrfid.biz.ideus.ericssonrfid.models.data_base.BatteryTypeEntity;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.checklist_screen.CheckListFragment;
import io.requery.query.Result;
import io.requery.query.Tuple;
import rx.Observable;

/**
 * Created by user on 28.12.2016.
 */

public class SummaryFragmentVM extends AbstractViewModel<IView> {
    static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    private Context context;
    private List<BatterySummaryParamsEntity> batterySummaryParamsEntityList;
    private SummaryAdapter adapter = new SummaryAdapter();
    private String siteId="", type;
    private int brand, cnt, capacity;

    SummaryAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
        super.onCreate(arguments, savedInstanceState);
        if (savedInstanceState != null) siteId = savedInstanceState.getString("Site");
        if (arguments != null) siteId = arguments.getString("Site");
        getSummaryBatteryParamsList();
        createSummaryList();
    }

    private void getSummaryBatteryParamsList() {
        batterySummaryParamsEntityList = EricssonRfidApp.getInstance().getDB()
                .select(BatterySummaryParamsEntity.class)
                .orderBy(BatterySummaryParamsEntity.NPP)
                .get().toList();
    }

    private void createSummaryList() {
        Result<Tuple> resultList = EricssonRfidApp.getInstance().getDB()
                .select(BatteryEntity.TYPE)
                .where(BatteryEntity.ROOM_ID.eq(siteId))
                .groupBy(BatteryEntity.TYPE)
                .get();

        List<Tuple> typesList =  resultList.toList();

        cnt = EricssonRfidApp.getInstance().getDB()
                .count(BatteryEntity.class)
                .where(BatteryEntity.ROOM_ID.eq(siteId))
                .get()
                .value();

        brand = 0;

        if (typesList.size() == 1) {
            type = typesList.get(0).get(0);
            BatteryTypeEntity brandEntity = EricssonRfidApp.getInstance().getDB()
                    .select(BatteryTypeEntity.class)
                    .where(BatteryTypeEntity.TYPE_NAME.eq(type))
                    .get().firstOrNull();
            if (brandEntity != null) {
                brand = brandEntity.getBrand();
                capacity = brandEntity.getCapacity() * cnt;
            } else {
                brand = 0;
            }
        }
        if (typesList.size() > 1) {
            type = typesList.get(0).get(0);
            for (int i = 1; i < typesList.size(); i++) {
                type = type + ", " + typesList.get(i).get(0);
            }
        }

        Observable.from(batterySummaryParamsEntityList)
                .map(this::calculate)
                .toList()
                .subscribe(batterySummaryParamsEntities -> {
                    batterySummaryParamsEntityList = batterySummaryParamsEntities;
                    adapter.notifyDataSetChanged();
                });
    }

    private BatterySummaryParamsEntity calculate(BatterySummaryParamsEntity item){

        if (item.getCalculations()) {
            String brandValue = brand == 1 ?  item.getBrand1Value() : item.getBrand2Value();
            if (brand != 0) {
                Float presentation = getFloatValue(brandValue) * cnt;
                if (presentation == 0.0) {
                    item.setPresentation(brandValue);
                } else {
                    item.setPresentation(presentation.toString());
                }
            } else {
                item.setPresentation("");
            }
        } else {
            item.setPresentation(item.getBrand1Value());
        }

        if (item.getDescription().equals("Battery Brand")) {
            item.setPresentation(type);
        }
        if (item.getDescription().equals("Number of Battery strings")) {
            item.setPresentation(String.valueOf(cnt));
        }
        if (item.getDescription().equals("Installation date") || item.getDescription().equals("Shipping date/scanning date")) {
            item.setPresentation(formatter.format(new Date()));
        }
        if (item.getDescription().equals("Battery Brand")) {
            if (brand == 0) {
                item.setLayoutType(2);
            } else {
                item.setLayoutType(0);
            }
        }
        if (item.getDescription().equals("Installed Battery Capacity") && brand!=0) {
            item.setPresentation(String.valueOf(capacity));
        }
        return item;
    }

    private float getFloatValue(String value) {
        float f;
        try {
            f = Float.parseFloat(value);
        } catch (Exception e) {
            f = 0.0f;
        }
        return f;
    }


    @Override
    public void onBindView(@NonNull IView view) {
        super.onBindView(view);
        context = view.getViewModelBindingConfig().getContext();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("Site", siteId);
    }

    private class SummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            RecyclerView.ViewHolder holder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            if (viewType == 0) {
                ItemSummaryOneBinding binding = DataBindingUtil.inflate(inflater,
                        R.layout.item_summary_one, parent, false);
                holder = new ItemOneHolder(binding.getRoot());
            }
            if (viewType == 1) {
                ItemSummaryTwoBinding binding = DataBindingUtil.inflate(inflater,
                        R.layout.item_summary_two, parent, false);
                holder = new ItemTwoHolder(binding.getRoot());
            }
            if (viewType == 2) {
                ItemSummaryThreeBinding binding = DataBindingUtil.inflate(inflater,
                        R.layout.item_summary_three, parent, false);
                holder = new ItemThreeHolder(binding.getRoot());
            }
            if (viewType == 3) {
                ItemSummaryFourBinding binding = DataBindingUtil.inflate(inflater,
                        R.layout.item_summary_four, parent, false);
                holder = new ItemFourHolder(binding.getRoot());
            }


            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            BatterySummaryParamsEntity batterySummaryParamsEntity = batterySummaryParamsEntityList.get(position);
            if (holder instanceof ItemOneHolder) {
                ((ItemOneHolder) holder).binding.setViewModel(batterySummaryParamsEntity);
            }
            if (holder instanceof ItemTwoHolder) {
                ((ItemTwoHolder) holder).binding.setViewModel(batterySummaryParamsEntity);
            }
            if (holder instanceof ItemThreeHolder) {
                ((ItemThreeHolder) holder).binding.setViewModel(batterySummaryParamsEntity);
            }
            if (holder instanceof ItemFourHolder) {
                ((ItemFourHolder) holder).binding.setViewModel(batterySummaryParamsEntity);
            }


        }

        @Override
        public int getItemCount() {
            return batterySummaryParamsEntityList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return batterySummaryParamsEntityList.get(position).getLayoutType();
        }
    }

    private static class ItemOneHolder extends RecyclerView.ViewHolder {
        ItemSummaryOneBinding binding;

        ItemOneHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }
    private static class ItemTwoHolder extends RecyclerView.ViewHolder {
        ItemSummaryTwoBinding binding;

        ItemTwoHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }
    private static class ItemThreeHolder extends RecyclerView.ViewHolder {
        ItemSummaryThreeBinding binding;

        ItemThreeHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }
    private static class ItemFourHolder extends RecyclerView.ViewHolder {
        ItemSummaryFourBinding binding;

        ItemFourHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }

    public void onContinueClick(View view){
        if (brand == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Attention")
                    .setMessage("All batteries site must be the same type")
                    .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_warning)
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.ok),
                            (dialog, id) -> dialog.dismiss());
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            ((MainActivity) context).addFragment(new CheckListFragment(), true);
        }
    }

}
