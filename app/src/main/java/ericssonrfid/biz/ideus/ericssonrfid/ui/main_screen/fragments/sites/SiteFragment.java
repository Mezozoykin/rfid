package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.sites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.adapters.SiteRecyclerAdapter;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.FragmentSiteBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarEnums;
import ericssonrfid.biz.ideus.ericssonrfid.models.data_base.DataBaseApi;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.SitesResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetApi;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetListener;
import ericssonrfid.biz.ideus.ericssonrfid.rx_buses.RxBusNewDataEvent;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.site_details_screen.SiteDetailsFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;
import ericssonrfid.biz.ideus.ericssonrfid.utils.Constants;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;




public class SiteFragment extends BaseFragment<IView, SiteFragmentVM, FragmentSiteBinding> implements IView, SiteRecyclerAdapter.OnSelectClickListener {

    private SiteRecyclerAdapter adapter;
    private List<SiteEntity> siteList = new ArrayList<>();
    protected Subscription rxBusAddNewSiteSubscription;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(this);
        initSiteList();
        fetchSites();
        rxBusAddNewSiteSubscription = getRxBusAddNewSiteSubscription();
    }

    private void initSiteList() {
        adapter = new SiteRecyclerAdapter(this, siteList);
        getBinding().rViewSite.setAdapter(adapter);
        getBinding().rViewSite.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarCreator.createToolbar(ToolbarEnums.SITE_AVAILABLE, null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (rxBusAddNewSiteSubscription != null && !rxBusAddNewSiteSubscription.isUnsubscribed())
            rxBusAddNewSiteSubscription.unsubscribe();
    }

    private void fetchSites() {
        new NetApi(mActivity)
                .setNetworkListener(new NetListener<SitesResponseData>() {
                    @Override
                    public void onSuccess(SitesResponseData responseData) {
                        storeSitiesToDB(responseData.getSiteEntities());
                    }
                }).getSites();
    }

    private void storeSitiesToDB(List<SiteEntity> siteEntities){
        DataBaseApi.getInstance().storeSities(siteEntities)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(siteEntities1 -> {
                    adapter.setSiteEntities((List<SiteEntity>) siteEntities1);
                });
    }

    private Subscription getRxBusAddNewSiteSubscription() {
        return RxBusNewDataEvent.instanceOf().getEvents().subscribe(event -> {
            if(event.equals(Constants.SUCCESS_ADD_SITE))
            fetchSites();
        });
    }

    @Nullable
    @Override
    public Class<SiteFragmentVM> getViewModelClass() {
        return SiteFragmentVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.fragment_site, BR.viewModel, getContext());
    }

    @Override
    public void onClickPosition(SiteEntity siteEntity) {
        mActivity.setSelectSiteEntity(siteEntity);
        mActivity.addFragment(new SiteDetailsFragment(), true);
    }
}
