package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;


public class SaveBatteryRequest extends BaseRequest<SaveBatteryParams> {

    public SaveBatteryRequest(String batteryId, boolean isUploadedPhoto, String siteId, String rfidNumber, String manufacturer
            , String type, String serialNumber, String shippingDate, String soldVendor, String chargeDate) {
        super(new SaveBatteryParams(batteryId, isUploadedPhoto, siteId, rfidNumber, manufacturer, type, serialNumber
                , shippingDate, soldVendor, chargeDate));
    }


    @Override
    public String getCommand() {
        return "battery";
    }
}

class SaveBatteryParams extends BaseRequestParams {

    @SerializedName("id")
    private String id;

    @SerializedName("siteId")
    private String siteId;

    @SerializedName("rfidNumber")
    private String rfidNumber;

    @SerializedName("manufacturer")
    private String manufacturer;

    @SerializedName("type")
    private String type;

    @SerializedName("serialNumber")
    private String serialNumber;

    @SerializedName("shippingDate")
    private String shippingDate;

    @SerializedName("soldVendor")
    private String soldVendor;

    @SerializedName("chargeDate")
    private String chargeDate;

    @SerializedName("files")
    private BatteryPhoto batteryPhoto;

    public SaveBatteryParams(String batteryId ,boolean isUploadedPhoto, String siteId, String rfidNumber, String manufacturer
            , String type, String serialNumber, String shippingDate, String soldVendor, String chargeDate) {


        this.batteryPhoto = new BatteryPhoto(isUploadedPhoto);
        this.id =  batteryId;
        this.siteId = siteId;
        this.rfidNumber = rfidNumber;
        this.manufacturer = manufacturer;
        this.type = type;
        this.serialNumber = serialNumber;
        this.shippingDate = shippingDate;
        this.soldVendor = soldVendor;
        this.chargeDate = chargeDate;

    }

public class BatteryPhoto{
    @SerializedName("batteryPhoto")
    private boolean isUploadPhoto;

    public BatteryPhoto(boolean isUploadPhoto) {
        this.isUploadPhoto = isUploadPhoto;
    }

}
}
