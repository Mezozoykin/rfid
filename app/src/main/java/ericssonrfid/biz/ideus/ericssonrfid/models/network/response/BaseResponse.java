package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BaseResponse<T extends BaseResponseData> {
    @SerializedName("data")
    private T data;

    @SerializedName("code")
    private int code;

    @SerializedName("errors")
    private ArrayList<String> errors;

    public int getCode() {
        return code;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }

    public T getData() {
        return data;
    }
}
