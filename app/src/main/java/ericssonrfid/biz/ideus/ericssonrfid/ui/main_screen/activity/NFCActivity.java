package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ActivityMainBinding;
import ericssonrfid.biz.ideus.ericssonrfid.nfc.factory.NDEFRecordFactory;
import ericssonrfid.biz.ideus.ericssonrfid.nfc.nfc_models.BaseRecord;
import ericssonrfid.biz.ideus.ericssonrfid.rx_buses.RxBusScanDialogEvent;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseActivity;
import rx.Subscription;


public abstract class NFCActivity extends BaseActivity<IView, MainActivityVM, ActivityMainBinding> implements IView {

    private Subscription scanDialogEvent;
    private ProgressDialog progressDialog;

    private PendingIntent nfcPendingIntent;
    private IntentFilter[] intentFiltersArray;
    private boolean isReadyReadNfc = false;
    private NfcAdapter nfcAdapter;

    abstract void setNfcData(BaseRecord result);

    @Nullable
    @Override
    public Class<MainActivityVM> getViewModelClass() {
        return MainActivityVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.activity_main, BR.viewModel, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setModelView(this);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        scanDialogEvent = getScanDialogEventSubscription();


        Intent nfcIntent = new Intent(this, getClass());
        nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        nfcPendingIntent = PendingIntent.getActivity(this, 0, nfcIntent, 0);

        // Create an Intent Filter limited to the URI or MIME type to
        // intercept TAG scans from.
        IntentFilter tagIntentFilter =
                new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            tagIntentFilter.addDataScheme("http");
            tagIntentFilter.addDataScheme("vnd.android.nfc");
            tagIntentFilter.addDataScheme("tel");
            tagIntentFilter.addDataType("text/plain");
            intentFiltersArray = new IntentFilter[]{tagIntentFilter};
        } catch (Throwable t) {
            t.printStackTrace();
        }

    }


    private void getTag(Intent i) {
        if (i == null)
            return;

        String type = i.getType();
        String action = i.getAction();
        List<BaseRecord> dataList = new ArrayList<BaseRecord>();

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Log.d("Nfc", "Action NDEF Found");
            Parcelable[] parcs = i.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            // List record

            for (Parcelable p : parcs) {
                NdefMessage msg = (NdefMessage) p;
                final int numRec = msg.getRecords().length;
                //  recNumberTxt.setText(String.valueOf(numRec));

                NdefRecord[] records = msg.getRecords();
                for (NdefRecord record : records) {
                    BaseRecord result = NDEFRecordFactory.createRecord(record);
                    if (result != null) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();

                        }
                        setNfcData(result);

                    }
                }
            }

        }

    }

    private boolean checkHasNfcAdapter() {
        if (nfcAdapter == null) {
            createDisableNFCDialog(false, getString(R.string.nfc_not_support_title)
                    , getString(R.string.nfc_not_support_message));
            return false;
        } else if (!nfcAdapter.isEnabled()) {
            createDisableNFCDialog(true, getString(R.string.please_enable_nfc_title)
                    , getString(R.string.please_enable_nfc_message));
            return false;
        }
        return true;
    }

    public void createDisableNFCDialog(boolean isHasNFC, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_warning)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                .setPositiveButton(getString(R.string.ok),
                        (dialog, id) -> {
                            if (isHasNFC) {
                                startActivity(new Intent(Settings.ACTION_NFC_SETTINGS));
                            }
                            dialog.dismiss();
                        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private Subscription getScanDialogEventSubscription() {
        return RxBusScanDialogEvent.instanceOf().getEvents().subscribe(s -> {
            switch (s) {
                case RFID:
                    if (checkHasNfcAdapter()) {
                        isReadyReadNfc = true;
                        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, intentFiltersArray, null);
                        createProgressDialog();
                    }
                    break;
                case QR_CODE:
                    break;
                case BLUTOOTH:
                    break;
            }
        });
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (isReadyReadNfc)
            getTag(intent);

    }

    private void handleIntent(Intent i) {
        getTag(i);
    }

    private void createProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage(getString(R.string.please_bring_your_device));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressNumberFormat(null);
        progressDialog.setProgressPercentFormat(null);
        progressDialog.setOnDismissListener(dialog -> isReadyReadNfc = false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, which) -> {
            progressDialog.dismiss();
        });
        progressDialog.show();


    }

    @Override
    protected void onPause() {
        super.onPause();
        if (nfcAdapter != null)
            nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (scanDialogEvent != null && !scanDialogEvent.isUnsubscribed())
            scanDialogEvent.unsubscribe();

    }

}
