package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;


public class GetBatteriesRequest extends BaseRequest<GetBatteriesParams> {

    public GetBatteriesRequest(String siteId) {
        super(new GetBatteriesParams(siteId));
    }

    @Override
    public String getCommand() {
        return "batteries";
    }
}

class GetBatteriesParams extends BaseRequestParams {

    @SerializedName("siteId")
    String siteId;

    public GetBatteriesParams(String siteId) {
        this.siteId = siteId;
    }

}

