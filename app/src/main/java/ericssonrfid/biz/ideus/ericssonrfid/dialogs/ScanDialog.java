package ericssonrfid.biz.ideus.ericssonrfid.dialogs;

import android.app.DialogFragment;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ScanDialogBinding;
import ericssonrfid.biz.ideus.ericssonrfid.enums.ScanDialogCommands;
import ericssonrfid.biz.ideus.ericssonrfid.rx_buses.RxBusScanDialogEvent;


public class ScanDialog  extends DialogFragment {

    private ScanDialogBinding binding;
    private static ScanDialog instance;


    public static ScanDialog getInstance() {
        if(instance == null){
            instance = new ScanDialog();
            return instance;
        } else {
            return instance;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setDialogParams();
        binding = DataBindingUtil.inflate(inflater, R.layout.scan_dialog, container, true);
        binding.setVariable(BR.viewModel, this);
        return binding.getRoot();
    }

    private void setDialogParams() {
        Window window = getDialog().getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.gravity = Gravity.BOTTOM;
        layoutParams.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(layoutParams);
    }


    public void onRFIDClick(View view) {
        RxBusScanDialogEvent.instanceOf().setRxBusScanDialogEvent(ScanDialogCommands.RFID);
        dismiss();
    }

    public void onQRClick(View view) {
        RxBusScanDialogEvent.instanceOf().setRxBusScanDialogEvent(ScanDialogCommands.QR_CODE);
        dismiss();
    }

    public void onBlutoothClick(View view) {
        RxBusScanDialogEvent.instanceOf().setRxBusScanDialogEvent(ScanDialogCommands.BLUTOOTH);
        dismiss();
    }

}




