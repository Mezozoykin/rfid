package ericssonrfid.biz.ideus.ericssonrfid.rx_buses;

import ericssonrfid.biz.ideus.ericssonrfid.enums.ScanDialogCommands;
import rx.Observable;
import rx.subjects.PublishSubject;


public class RxBusScanDialogEvent {

        private static RxBusScanDialogEvent instance;

        private PublishSubject<ScanDialogCommands> subject = PublishSubject.create();

        public static RxBusScanDialogEvent instanceOf() {
            if (instance == null) {
                instance = new RxBusScanDialogEvent();
            }
            return instance;
        }

        public void setRxBusScanDialogEvent(ScanDialogCommands event) {
            subject.onNext(event);
        }
        public void onCompleted() {
            subject.onCompleted();
        }


        public Observable<ScanDialogCommands> getEvents() {
            return subject;
        }
    }


