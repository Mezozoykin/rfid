package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;

/**
 * Created by user on 29.12.2016.
 */

public class BatteryListResponse extends BaseResponseData {
    @SerializedName("batteries")
    private List<BatteryEntity> batteryEntityList;

    public List<BatteryEntity> getBatteryEntityList() {
        return batteryEntityList;
    }

    public void setBatteryEntityList(List<BatteryEntity> batteryEntityList) {
        this.batteryEntityList = batteryEntityList;
    }
}