package ericssonrfid.biz.ideus.ericssonrfid.rx_buses;

import rx.Observable;
import rx.subjects.PublishSubject;



public class RxBusNewDataEvent {
    private static RxBusNewDataEvent instance;

    private PublishSubject<String> subject = PublishSubject.create();

    public static RxBusNewDataEvent instanceOf() {
        if (instance == null) {
            instance = new RxBusNewDataEvent();
        }
        return instance;
    }

    public void setRxBusNewDataEvent(String event) {
        subject.onNext(event);
    }
    public void onCompleted() {
        subject.onCompleted();
    }


    public Observable<String> getEvents() {
        return subject;
    }
}
