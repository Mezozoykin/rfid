package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

public class UploadPhotoData extends BaseResponseData {
    @SerializedName("index")
    private Integer index;
    @SerializedName("total")
    private Integer total;
    @SerializedName("file")
    private String file;
    @SerializedName("tmpfile")
    private String tmpfile;

    public Integer getIndex() {
        return index;
    }

    public Integer getTotal() {
        return total;
    }

    public String getFile() {
        return file;
    }

    public String getTmpfile() {
        return tmpfile;
    }
}



