package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;


public class BatteriesResponseData extends BaseResponseData {
    @SerializedName("batteries")
    private List<BatteryEntity> batteryEntities;

    public List<BatteryEntity> getBatteryEntities() {
        return batteryEntities;
    }

    public void setBatteryEntities(List<BatteryEntity> batteryEntities) {
        this.batteryEntities = batteryEntities;
    }
}
