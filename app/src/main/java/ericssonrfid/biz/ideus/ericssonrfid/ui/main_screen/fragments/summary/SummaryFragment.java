package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.summary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.FragmentSummaryRvBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarEnums;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;

/**
 * Created by user on 28.12.2016.
 */

public class SummaryFragment extends BaseFragment<IView, SummaryFragmentVM, FragmentSummaryRvBinding> implements IView{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBinding().rv.setLayoutManager(new LinearLayoutManager(mActivity));
        getBinding().rv.setAdapter(getViewModel().getAdapter());

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(this);
        RecyclerView rv = getBinding().rv;
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                DividerItemDecoration.VERTICAL);
        rv.addItemDecoration(dividerItemDecoration);

        getBinding().rv.setAdapter(getViewModel().getAdapter());
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.fragment_summary_rv, BR.viewModel, getContext());
    }

    @Nullable
    @Override
    public Class<SummaryFragmentVM> getViewModelClass() {
        return SummaryFragmentVM.class;
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarCreator.createToolbar(ToolbarEnums.BASE_TOOLBAR, getString(R.string.summary));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ToolbarCreator.createToolbar(ToolbarEnums.SITE_DETAILS, null);
    }
}
