package ericssonrfid.biz.ideus.ericssonrfid.models.data_base;

import android.databinding.Observable;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by user on 27.12.2016.
 */
@Entity
public interface BatterySummaryParams extends Observable, Parcelable, Persistable {

    @Key
    @Generated
    int getId();

    String getDescription();
    void setDescription(String description);

    String getGroupDescription();
    void setGroupDescription(String groupDescription);

    String getShortDescription();
    void setShortDescription(String shortDescription);

    String getBrand1Value();
    void setBrand1Value(String value);

    String getBrand2Value();
    void setBrand2Value(String value);

    boolean getCalculations();
    void setCalculations(boolean isCalculations);

    String getPresentation();
    void setPresentation(String presentation);

    int getLayoutType();
    void setLayoutType(int layoutType);

    int getNpp();
    void setNpp(int npp);
}
