package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.add_site_screen;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.databinding.ObservableField;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import biz.ideus.ideuslib.Utils.AndroidPermissions;
import biz.ideus.ideuslib.mvvm_lifecycle.AbstractViewModel;
import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetApi;
import ericssonrfid.biz.ideus.ericssonrfid.rx_buses.RxBusNewDataEvent;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;
import ericssonrfid.biz.ideus.ericssonrfid.utils.Constants;
import ericssonrfid.biz.ideus.ericssonrfid.utils.LocationFinder;


public class AddSiteFragmentVM extends AbstractViewModel<IView> implements LocationFinder.FindLocationListener, MainActivity.OnRequestPermissionsListener {

    private String nameSite = "";
    private String commentSite = "";
    private Location currentLocation;
    private MainActivity activity;

    public static final int PERMISSION_REQUEST_CODE_LOCATION = 1111;
    private AndroidPermissions androidPermissions;
    private LocationFinder locationFinder;
    private boolean isOpenGpsSettings = false;

    public ObservableField<String> locationTitle = new ObservableField<>();
    public ObservableField<Integer> visibilityProgress = new ObservableField<>();


    @Override
    public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
        super.onCreate(arguments, savedInstanceState);
        visibilityProgress.set(View.VISIBLE);

    }

    @Override
    public void onBindView(@NonNull IView view) {
        super.onBindView(view);
        activity = (MainActivity) view.getViewModelBindingConfig().getContext();
        activity.setOnRequestPermissionsListener(this);

        locationFinder = new LocationFinder(activity, this);
        androidPermissions = new AndroidPermissions(activity
                , Manifest.permission.ACCESS_FINE_LOCATION
                , Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (androidPermissions.checkPermissions()) {
            locationFinder.fetchLocationData();
        } else {
            androidPermissions.requestPermissions(PERMISSION_REQUEST_CODE_LOCATION);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (androidPermissions.checkPermissions()) {
            locationFinder.removeLocationUpdates();
        }
    }



    private void openGpsSettings() {
        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivity(callGPSSettingIntent);
        isOpenGpsSettings = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (androidPermissions.areAllRequiredPermissionsGranted(permissions, grantResults)) {
            locationFinder.fetchLocationData();
        }
    }


    public void onTextChangedName(CharSequence text, int start, int before, int count) {
        nameSite = text.toString();

    }

    public void onTextChangedComment(CharSequence text, int start, int before, int count) {
        commentSite = text.toString();

    }

    private boolean isValidFields() {
        return !nameSite.isEmpty() && currentLocation != null;
    }

    public void onClickSave(View view) {
        if (isValidFields()) {
            addSite();
        } else {
            activity.createErrorDialog(activity.getString(R.string.invalid_field)
                    , activity.getString(R.string.invalid_fields_addsite));
        }
    }

    private void addSite() {
        String comment = (!commentSite.isEmpty()) ? commentSite : null;
        new NetApi(activity)
                .setNetworkListener(responseData -> {
                    RxBusNewDataEvent.instanceOf().setRxBusNewDataEvent(Constants.SUCCESS_ADD_SITE);
                    activity.hideKeyboard();
                    activity.getSupportFragmentManager().popBackStack();
                }).addSite(nameSite, comment, String.valueOf(currentLocation.getLatitude())
                , String.valueOf(currentLocation.getLongitude()));
    }

    @Override
    public void onProviderLocationChanged(Location location) {
        currentLocation = location;
        if (currentLocation != null) {
            visibilityProgress.set(View.GONE);
            locationTitle.set(String.valueOf(currentLocation.getLatitude() + " | " + String.valueOf(currentLocation.getLongitude())));
        }
    }

    @Override
    public void onProviderLocationDisable() {
        if (!isOpenGpsSettings) {
            createDisableGpsDialog();
        }
    }

    public void createDisableGpsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.disable_gps_title))
                .setMessage(activity.getString(R.string.disable_gps_message))
                .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_warning)
                .setCancelable(false)
                .setNegativeButton(activity.getString(R.string.cancel), (dialog, which) -> dialog.dismiss())
                .setPositiveButton(activity.getString(R.string.open_gps_settings),
                        (dialog, id) -> {
                            openGpsSettings();
                            dialog.dismiss();
                        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
