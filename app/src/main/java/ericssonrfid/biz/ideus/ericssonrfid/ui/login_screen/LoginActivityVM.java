package ericssonrfid.biz.ideus.ericssonrfid.ui.login_screen;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.util.concurrent.TimeUnit;

import biz.ideus.ideuslib.Utils.UtilsValidationETFields;
import biz.ideus.ideuslib.mvvm_lifecycle.AbstractViewModel;
import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.AccountLoginResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetApi;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetListener;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;
import ericssonrfid.biz.ideus.ericssonrfid.utils.Constants;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;


public class LoginActivityVM extends AbstractViewModel<IView> {

    public static final int MIN_COUNT_CHARACTER_PASSWORD = 5;
    private Context context;
    private boolean isValidEmail = false;
    private boolean isValidPassword = false;


    public final ObservableField<CharSequence> email = new ObservableField<>();
    public final ObservableField<CharSequence> password = new ObservableField<>();
    public final ObservableField<Integer> titleColorEmail = new ObservableField<>();
    public final ObservableField<Integer> titleColorPassword = new ObservableField<>();

    @Override
    public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
        super.onCreate(arguments, savedInstanceState);

    }

//"test.user1@tst.tst", "123456");
    @Override
    public void onBindView(@NonNull IView view) {
        super.onBindView(view);
        context = view.getViewModelBindingConfig().getContext();
    }

    public void onClickLogin(View view) {
        if (isValidFields()) {
            login(email.get().toString(), password.get().toString());

        } else {
            ((LoginActivity)context).createErrorDialog(context.getString(R.string.invalid_field)
                    , context.getString(R.string.invalidate_login_text));
        }
    }

    private void login(String email, String password) {
        new NetApi((LoginActivity)context)
                .setNetworkListener(new NetListener<AccountLoginResponseData>() {
                    @Override
                    public void onSuccess(AccountLoginResponseData responseData) {
                        EricssonRfidApp.getInstance().saveAuthToken(responseData.getToken());
                        EricssonRfidApp.getInstance().saveUserAccId(responseData.getAccId());
                        goToSiteScreen();
                    }
                }).login(email, password);
    }

    private void goToSiteScreen() {
        LoginActivity loginActivity = (LoginActivity) context;
        loginActivity.startActivity(new Intent(loginActivity, MainActivity.class));
        loginActivity.finish();
    }

    public void onTextChangedEmail(CharSequence text, int start, int before, int count) {
        email.set(text);
        Observable.just(text.toString())
                .debounce(500, TimeUnit.MILLISECONDS)
                .flatMap(currentText -> Observable.just(UtilsValidationETFields.validateEmail(currentText, Constants.EMAIL_PATTERN)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    isValidEmail = aBoolean;
                    titleColorEmail.set(getColor(aBoolean, context));
                });
    }

    public void onTextChangedPassword(CharSequence text, int start, int before, int count) {
        password.set(text);
        Observable.just(text.toString())
                .debounce(500, TimeUnit.MILLISECONDS)
                .flatMap(currentText -> Observable.just(UtilsValidationETFields.validatePassword(currentText, MIN_COUNT_CHARACTER_PASSWORD)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    isValidPassword = aBoolean;
                    titleColorPassword.set(getColor(aBoolean, context));
                });
    }
    
    private boolean isValidFields() {
        return isValidEmail && isValidPassword;

    }

    private int getColor(Boolean isValid, Context context) {
        return (isValid) ? ContextCompat.getColor(context, biz.ideus.ideuslib.R.color.black)
                : ContextCompat.getColor(context, biz.ideus.ideuslib.R.color.error_color);
    }



}
