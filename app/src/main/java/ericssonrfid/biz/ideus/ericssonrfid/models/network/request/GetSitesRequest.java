package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

/**
 * Created by walkmn on 26.12.16.
 */

public class GetSitesRequest extends BaseRequest<BaseRequestParams> {

    public GetSitesRequest() {
        super(new BaseRequestParams());
    }

    @Override
    public String getCommand() {
        return "sites";
    }
}

