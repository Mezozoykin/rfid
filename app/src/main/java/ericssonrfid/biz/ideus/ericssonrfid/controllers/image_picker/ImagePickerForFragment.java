package ericssonrfid.biz.ideus.ericssonrfid.controllers.image_picker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.FileNotFoundException;

import ericssonrfid.biz.ideus.ericssonrfid.utils.Utils;

import static android.app.Activity.RESULT_OK;

public class ImagePickerForFragment {

    private static final int REQUEST_CAMERA_PERMISSIONS = 931;

    private Fragment fragment;
    private ImagePickerListener mImagePickerListener;

    private boolean withCrooper = false;

    public void setWithCrooper(boolean withCrooper) {
        this.withCrooper = withCrooper;
    }


    public ImagePickerForFragment(Fragment fragment, ImagePickerListener imagePickerListener) {
        this.fragment = fragment;
        this.mImagePickerListener = imagePickerListener;
    }

    public void pickImage() {

        if (ContextCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(fragment.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fragment.requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSIONS);
            }
            return;
        }

        showUploadPhotoDialog();
    }

    private void showUploadPhotoDialog() {
        Intent chooseImageIntent = CropImage.getPickImageChooserIntent(fragment.getContext());
        fragment.startActivityForResult(chooseImageIntent, CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSIONS:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    showUploadPhotoDialog();
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {

        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {

            case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
                Uri resultPick = CropImage.getPickImageResultUri(fragment.getContext(), imageReturnedIntent);
                if (resultPick != null) {
                    if (withCrooper) {
                        CropImage.activity(resultPick).start(fragment.getContext(), fragment);
                        return;
                    }
                    tryToSendResult(resultPick);
                    try {
                        Bitmap bitmap = Utils.decodeUri(fragment.getContext(), resultPick);
                        mImagePickerListener.onGotImage(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(imageReturnedIntent);
                try {
                    Bitmap bitmap = Utils.decodeUri(fragment.getContext(), result.getUri());
                    if (bitmap == null) {
                        return;
                    }
                    mImagePickerListener.onGotImage(bitmap);
                    tryToSendResult(result.getUri());

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

    private void tryToSendResult(Uri result) {
        if (result == null) {
            return;
        }

        if (mImagePickerListener instanceof ImagePickerExpListener) {
            ((ImagePickerExpListener) mImagePickerListener).onGotResult(result);
        }
    }
}
