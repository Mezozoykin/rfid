package ericssonrfid.biz.ideus.ericssonrfid.controllers.image_picker;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.theartofdev.edmodo.cropper.CropImage;

import java.io.FileNotFoundException;

import ericssonrfid.biz.ideus.ericssonrfid.utils.Utils;

import static android.app.Activity.RESULT_OK;

public class ImagePickerForActivity {

    private static final int REQUEST_CAMERA_PERMISSIONS = 931;

    private Activity mActivity;
    private ImagePickerListener mImagePickerListener;

    public ImagePickerForActivity(Activity activity, ImagePickerListener imagePickerListener) {
        this.mActivity = activity;
        this.mImagePickerListener = imagePickerListener;
    }

    public void pickImage() {

        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mActivity.requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSIONS);
            }
            return;
        }

        showUploadPhotoDialog();
    }

    private void showUploadPhotoDialog() {
        Intent chooseImageIntent = CropImage.getPickImageChooserIntent(mActivity);
        mActivity.startActivityForResult(chooseImageIntent, CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showUploadPhotoDialog();
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {

        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {

            case CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE:
                Uri resultPick = CropImage.getPickImageResultUri(mActivity, imageReturnedIntent);
                if (resultPick != null) {
                    CropImage.activity(resultPick).start(mActivity);
                }
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(imageReturnedIntent);
                try {
                    Bitmap bitmap = Utils.decodeUri(mActivity, result.getUri());
                    if (bitmap == null) {
                        return;
                    }
                    mImagePickerListener.onGotImage(bitmap);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }
}
