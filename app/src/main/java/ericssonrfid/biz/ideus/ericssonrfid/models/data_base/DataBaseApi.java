package ericssonrfid.biz.ideus.ericssonrfid.models.data_base;

import android.util.Log;

import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;
import io.requery.Persistable;
import io.requery.rx.SingleEntityStore;
import rx.Observable;


public class DataBaseApi {
    SingleEntityStore<Persistable> data = EricssonRfidApp.getInstance().getDB();
    private static DataBaseApi instance;


    public static DataBaseApi getInstance() {
        if(instance == null){
            instance = new DataBaseApi();
            return instance;
        } else {
            return instance;
        }
    }


    public Observable<Iterable<SiteEntity>> storeSities(List<SiteEntity> siteEntities) {
        int cnt = data.toBlocking().delete(SiteEntity.class).get().value();
        Log.d("database_del", cnt + "");
        return data.insert(siteEntities).toObservable();
    }

    public Observable<Iterable<BatteryEntity>> storeBatteries(List<BatteryEntity> batteryEntities) {
        int cnt = data.toBlocking().delete(BatteryEntity.class).get().value();
        Log.d("database_del", cnt + "");
        return data.insert(batteryEntities).toObservable();
    }
}
