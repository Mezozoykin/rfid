package ericssonrfid.biz.ideus.ericssonrfid.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemChecklistBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.checklist_screen.CheckListModel;


public class CheckListAdapter extends RecyclerView.Adapter<CheckListAdapter.CheckListHolder> {

    private Context context;
    private List<CheckListModel> checkListModels;

    public CheckListAdapter(Context context) {
        this.context = context;
        checkListModels = getCheckListModels();
    }

    @Override
    public CheckListAdapter.CheckListHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new CheckListAdapter.CheckListHolder(DataBindingUtil.inflate(inflater,
                R.layout.item_checklist, parent, false).getRoot());

    }

    @Override
    public void onBindViewHolder(CheckListAdapter.CheckListHolder holder, int position) {
        CheckListModel checkListModel = checkListModels.get(position);
        holder.binding.tvCheckTitle.setText(checkListModel.getTitle());
        holder.binding.checkbox.setChecked(checkListModel.isChecked());
        holder.binding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> onCheckListener(position, isChecked));
        paintStrike(holder, checkListModel);
    }

    private void paintStrike(CheckListHolder holder, CheckListModel checkListModel) {
        if (checkListModel.isChecked()) {
            holder.binding.tvCheckTitle.setPaintFlags(holder.binding.tvCheckTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.binding.tvCheckTitle.setPaintFlags(0);
        }
    }


    @Override
    public int getItemCount() {
        return checkListModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public static class CheckListHolder extends RecyclerView.ViewHolder {
        ItemChecklistBinding binding;

        public CheckListHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }

    private void onCheckListener(int position, boolean check) {
        checkListModels.get(position).setChecked(check);

        notifyDataSetChanged();
    }

    public boolean isAllCheck() {
        boolean result = true;
        for (CheckListModel checkListModel : checkListModels ) {
            if (!checkListModel.isChecked()) result = false;
        }
        return result;
//        final boolean[] isCheck = {true};
//        Observable.from(checkListModels)
//                .flatMap(item -> Observable.just(item.isChecked()))
//                .filter(aBoolean -> !aBoolean).map(aBoolean -> {
//            isCheck[0] = aBoolean;
//            return aBoolean;
//        }).subscribe();
//        return isCheck[0];
    }

    private List<CheckListModel> getCheckListModels() {
        List<CheckListModel> listModels = new ArrayList<>();
        List<String> titleList = new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.checkStringArray)));
        for (int i = 0; i < titleList.size(); i++) {
            listModels.add(new CheckListModel(titleList.get(i), false));
        }
        return listModels;

    }
}

