package ericssonrfid.biz.ideus.ericssonrfid.models.data_base;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;

/**
 * Created by user on 27.12.2016.
 */

public class FillBatteryDictionary {

    public static void delete(){
        int a;
        a = EricssonRfidApp.getInstance().getDB().delete(BatteryTypeEntity.class).get().value();
        Log.d("DATA", "BatteryTypeEntity deleted - " + a);
        a = EricssonRfidApp.getInstance().getDB().delete(BatterySummaryParamsEntity.class).get().value();
        Log.d("DATA", "BatterySummaryParamsEntity deleted - " + a);

    }

    public static  void fill() {
        fillTypes();
        fillSummary();
    }

    private static  void fillSummary() {
        List<BatterySummaryParamsEntity> list = new ArrayList<>();
        BatterySummaryParamsEntity batterySummaryParamsEntity;

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Description");
        batterySummaryParamsEntity.setGroupDescription("");
        batterySummaryParamsEntity.setBrand1Value("Presentation");
        batterySummaryParamsEntity.setBrand2Value("Presentation");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(3);
        batterySummaryParamsEntity.setNpp(1);
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Installation date");
        batterySummaryParamsEntity.setGroupDescription("");
        batterySummaryParamsEntity.setBrand1Value("02/06/2016");
        batterySummaryParamsEntity.setBrand2Value("02/06/2016");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(2);
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Number of Battery strings");
        batterySummaryParamsEntity.setGroupDescription("1");
        batterySummaryParamsEntity.setBrand1Value("1");
        batterySummaryParamsEntity.setBrand2Value("1");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(3);
        list.add(batterySummaryParamsEntity);


        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Installed Battery Capacity");
        batterySummaryParamsEntity.setGroupDescription("");
        batterySummaryParamsEntity.setBrand1Value("");
        batterySummaryParamsEntity.setBrand2Value("");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("Ah");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(4);
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Battery Brand");
        batterySummaryParamsEntity.setGroupDescription("");
        batterySummaryParamsEntity.setBrand1Value("");
        batterySummaryParamsEntity.setBrand2Value("");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(5);
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Shipping date/scanning date");
        batterySummaryParamsEntity.setGroupDescription("");
        batterySummaryParamsEntity.setBrand1Value("01/03/2016");
        batterySummaryParamsEntity.setBrand2Value("01/03/2016");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(6);
        list.add(batterySummaryParamsEntity);



        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Parameter settings");
        batterySummaryParamsEntity.setGroupDescription("Presentation");
        batterySummaryParamsEntity.setBrand1Value("Presentation");
        batterySummaryParamsEntity.setBrand2Value("Presentation");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(3);
        batterySummaryParamsEntity.setNpp(99);
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("High Battery Temperature Disconnect");
        batterySummaryParamsEntity.setGroupDescription("BattDiscTemp");
        batterySummaryParamsEntity.setBrand1Value("60");
        batterySummaryParamsEntity.setBrand2Value("60");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(°C)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(100);
 //       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("High Battery Temperature Disconnect  Seize Offset");
        batterySummaryParamsEntity.setGroupDescription("BattDiscCeaseOffset");
        batterySummaryParamsEntity.setBrand1Value("5");
        batterySummaryParamsEntity.setBrand2Value("5");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(°C)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(101);
        //       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Temperature Compensating Slope");
        batterySummaryParamsEntity.setGroupDescription("TempCompVoltSlope");
        batterySummaryParamsEntity.setBrand1Value("-12");
        batterySummaryParamsEntity.setBrand2Value("-12");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(mV)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(102);
        //       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Nominal Temperature");
        batterySummaryParamsEntity.setGroupDescription("NominalTemp");
        batterySummaryParamsEntity.setBrand1Value("25");
        batterySummaryParamsEntity.setBrand2Value("25");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(°C)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(103);
        //       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Charging Voltage");
        batterySummaryParamsEntity.setGroupDescription("CharginVoltage");
        batterySummaryParamsEntity.setBrand1Value("13.62");
        batterySummaryParamsEntity.setBrand2Value("13.62");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(104);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Boost charge/Equalize charge Voltage");
        batterySummaryParamsEntity.setGroupDescription("IncrChargeVolt");
        batterySummaryParamsEntity.setBrand1Value("14.32");
        batterySummaryParamsEntity.setBrand2Value("14.1");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(105);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Boost charge Time after reaching system voltage");
        batterySummaryParamsEntity.setGroupDescription("BoostChargeTime");
        batterySummaryParamsEntity.setBrand1Value("4");
        batterySummaryParamsEntity.setBrand2Value("3");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(h)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(106);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Discharge voltage to trigger Boost charge");
        batterySummaryParamsEntity.setGroupDescription("BoostTriggerVolt");
        batterySummaryParamsEntity.setBrand1Value("12.25");
        batterySummaryParamsEntity.setBrand2Value("12.25");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(107);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Calendar wise Equalize Charge Time");
        batterySummaryParamsEntity.setGroupDescription("EqualizeChargeTime");
        batterySummaryParamsEntity.setBrand1Value("4");
        batterySummaryParamsEntity.setBrand2Value("4");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(108);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Calendar wise Equalize Charge Interval");
        batterySummaryParamsEntity.setGroupDescription("EqualizeInterval");
        batterySummaryParamsEntity.setBrand1Value("30");
        batterySummaryParamsEntity.setBrand2Value("30");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(Days)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(109);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Main Load Voltage Disconnect");
        batterySummaryParamsEntity.setGroupDescription("MainLoadUVDisc");
        batterySummaryParamsEntity.setBrand1Value("");
        batterySummaryParamsEntity.setBrand2Value("");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(1);
        batterySummaryParamsEntity.setNpp(110);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("-   Stable grid");
        batterySummaryParamsEntity.setGroupDescription("MainLoadUVDisc");
        batterySummaryParamsEntity.setBrand1Value("10.9");
        batterySummaryParamsEntity.setBrand2Value("10.9");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(111);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("-   Unstable grid/Hybrid");
        batterySummaryParamsEntity.setGroupDescription("MainLoadUVDisc");
        batterySummaryParamsEntity.setBrand1Value("NA");
        batterySummaryParamsEntity.setBrand2Value("11.5");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(112);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Low Voltage Disconnect");
        batterySummaryParamsEntity.setGroupDescription("PrioLoadUVDisc");
        batterySummaryParamsEntity.setBrand1Value("");
        batterySummaryParamsEntity.setBrand2Value("");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(1);
        batterySummaryParamsEntity.setNpp(113);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("-   Stable grid");
        batterySummaryParamsEntity.setGroupDescription("PrioLoadUVDisc");
        batterySummaryParamsEntity.setBrand1Value("10.8");
        batterySummaryParamsEntity.setBrand2Value("10.8");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(114);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("-   Unstable grid/Hybrid");
        batterySummaryParamsEntity.setGroupDescription("PrioLoadUVDisc");
        batterySummaryParamsEntity.setBrand1Value("NA");
        batterySummaryParamsEntity.setBrand2Value("11.25");
        batterySummaryParamsEntity.setCalculations(true);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(115);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Min charge current/Stable grid");
        batterySummaryParamsEntity.setGroupDescription("System design");
        batterySummaryParamsEntity.setBrand1Value("0.1C");
        batterySummaryParamsEntity.setBrand2Value("0.1C");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(116);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Min charge current/Unstable grid");
        batterySummaryParamsEntity.setGroupDescription("System design");
        batterySummaryParamsEntity.setBrand1Value("NA");
        batterySummaryParamsEntity.setBrand2Value("0.25C");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(117);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Max charge current/Stable grid");
        batterySummaryParamsEntity.setGroupDescription("System design");
        batterySummaryParamsEntity.setBrand1Value("Unlimited");
        batterySummaryParamsEntity.setBrand2Value("Unlimited");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(118);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Max charge current/Unstable grid");
        batterySummaryParamsEntity.setGroupDescription("System design");
        batterySummaryParamsEntity.setBrand1Value("0.4C");
        batterySummaryParamsEntity.setBrand2Value("0.4C");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(119);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("End of discharge, Hybrid mode (DoD)");
        batterySummaryParamsEntity.setGroupDescription("System setting");
        batterySummaryParamsEntity.setBrand1Value("NA");
        batterySummaryParamsEntity.setBrand2Value("65%");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(120);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("End of Recharge, Hybrid mode (SOC)");
        batterySummaryParamsEntity.setGroupDescription("System setting");
        batterySummaryParamsEntity.setBrand1Value("NA");
        batterySummaryParamsEntity.setBrand2Value("95%");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(121);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Intermittent charging time on OCV (h)*");
        batterySummaryParamsEntity.setGroupDescription("IntermittChargeConnTime");
        batterySummaryParamsEntity.setBrand1Value("0");
        batterySummaryParamsEntity.setBrand2Value("0");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(h)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(122);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Intermittent charging time on charge (including Equalize charge time) (h)");
        batterySummaryParamsEntity.setGroupDescription("IntermittChargeDiscTime");
        batterySummaryParamsEntity.setBrand1Value("0");
        batterySummaryParamsEntity.setBrand2Value("0");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(h)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(123);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        batterySummaryParamsEntity = new BatterySummaryParamsEntity();
        batterySummaryParamsEntity.setDescription("Min OCV during intermittent charge on OCV (V)");
        batterySummaryParamsEntity.setGroupDescription("IntermittChargeConnVolt");
        batterySummaryParamsEntity.setBrand1Value("50");
        batterySummaryParamsEntity.setBrand2Value("50");
        batterySummaryParamsEntity.setCalculations(false);
        batterySummaryParamsEntity.setShortDescription("(V)");
        batterySummaryParamsEntity.setLayoutType(0);
        batterySummaryParamsEntity.setNpp(124);
//       EricssonRfidApp.getInstance().getDB().insert(batterySummaryParamsEntity).subscribe();
        list.add(batterySummaryParamsEntity);

        EricssonRfidApp.getInstance().getDB().insert(list).subscribe();



    }

    private static void fillTypes() {
        List<BatteryTypeEntity> batteryTypeEntityList = new ArrayList<>();
        BatteryTypeEntity batteryTypeEntity;

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 13 Red");
        batteryTypeEntity.setBrand(1);
        batteryTypeEntity.setCapacity(12);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(300);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 40 Red");
        batteryTypeEntity.setBrand(1);
        batteryTypeEntity.setCapacity(40);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(300);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 60 Red");
        batteryTypeEntity.setBrand(1);
        batteryTypeEntity.setCapacity(60);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(300);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 100 Red");
        batteryTypeEntity.setBrand(1);
        batteryTypeEntity.setCapacity(100);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(300);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 190 Red");
        batteryTypeEntity.setBrand(1);
        batteryTypeEntity.setCapacity(190);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(300);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 210 Red");
        batteryTypeEntity.setBrand(1);
        batteryTypeEntity.setCapacity(210);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(300);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 40 Blue+");
        batteryTypeEntity.setBrand(2);
        batteryTypeEntity.setCapacity(38);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(1200);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 100Blue+");
        batteryTypeEntity.setBrand(2);
        batteryTypeEntity.setCapacity(100);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(1200);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 190Blue+");
        batteryTypeEntity.setBrand(2);
        batteryTypeEntity.setCapacity(183);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(1200);
        batteryTypeEntityList.add(batteryTypeEntity);

        batteryTypeEntity = new BatteryTypeEntity();
        batteryTypeEntity.setTypeName("NSB 210Blue+");
        batteryTypeEntity.setBrand(2);
        batteryTypeEntity.setCapacity(200);
        batteryTypeEntity.setDesignLife(12);
        batteryTypeEntity.setCycleLife(1200);
        batteryTypeEntityList.add(batteryTypeEntity);
        
        EricssonRfidApp.getInstance().getDB().insert(batteryTypeEntityList).subscribe();
    }
}
