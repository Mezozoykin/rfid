package ericssonrfid.biz.ideus.ericssonrfid.network;

/**
 * Created by walkmn on 26.12.16.
 */

public interface ServerCatchErrorListener<T> {
    void onCatchError(int codeError, T response);
}
