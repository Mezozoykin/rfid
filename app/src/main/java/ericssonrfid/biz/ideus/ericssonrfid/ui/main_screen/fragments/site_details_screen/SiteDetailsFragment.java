package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.site_details_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.adapters.BatteriesRecyclerAdapter;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.FragmentSiteDetailsBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarEnums;
import ericssonrfid.biz.ideus.ericssonrfid.models.data_base.DataBaseApi;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.BatteriesResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetApi;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetListener;
import ericssonrfid.biz.ideus.ericssonrfid.rx_buses.RxBusNewDataEvent;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.battery_details_screen.BatteryDetailsFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.battery_details_screen.BatteryDetailsModel;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;
import ericssonrfid.biz.ideus.ericssonrfid.utils.Constants;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class SiteDetailsFragment extends BaseFragment<IView, SiteDetailsFragmentVM, FragmentSiteDetailsBinding>
        implements IView, BatteriesRecyclerAdapter.OnSelectBatteryClickListener {


    private BatteriesRecyclerAdapter adapter;
    private List<BatteryEntity> batteryEntityList = new ArrayList<>();
    protected Subscription rxBusAddNewBatterySubscription;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(this);
        fetchBatteries(mActivity.getSelectSiteEntity().getId());
        initBatteriesList();
        rxBusAddNewBatterySubscription = getRxBusAddNewBatterySubscription();

    }

    private void initBatteriesList() {
        adapter = new BatteriesRecyclerAdapter(this, batteryEntityList);
        getBinding().rViewBatteries.setAdapter(adapter);
        getBinding().rViewBatteries.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT ) {
            //| ItemTouchHelper.DOWN | ItemTouchHelper.UP
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Toast.makeText(EricssonRfidApp.getInstance(), "on Swiped ", Toast.LENGTH_SHORT).show();
                //Remove swiped item from list and notify the RecyclerView


                showDismissDialog( Integer.valueOf(((BatteriesRecyclerAdapter.BatteryItemHolder)viewHolder).binding.getViewModel().getId())
                , viewHolder.getAdapterPosition());

            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(getBinding().rViewBatteries);
    }

    private Subscription getRxBusAddNewBatterySubscription() {
        return RxBusNewDataEvent.instanceOf().getEvents().subscribe(event -> {
            if (event.equals(Constants.SUCCESS_SAVE_BATTERY))
                fetchBatteries(mActivity.getSelectSiteEntity().getId());
        });
    }

    void showDismissDialog(int batteryId, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Attention")
                .setMessage("Delete battery " + String.valueOf(position+1) + "# ?")
                .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_warning)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel),
                        (dialog, which) -> {
                            dialog.dismiss();
                            adapter.notifyItemChanged(position);
                        }
                )
                .setPositiveButton(getString(R.string.ok),
                        (dialog, id) -> {
                            deleteBattery(batteryId, position);
                            dialog.dismiss();
                        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void deleteBattery(int id, int position) {
        new NetApi(mActivity)
                .setNetworkListener(responseData -> {
                    EricssonRfidApp.getInstance().getDB()
                            .delete(BatteryEntity.class)
                            .where(BatteryEntity.ID.eq(String.valueOf(id)))
                            .get().value();
                    adapter.getBatteryEntities().remove(position);
                    adapter.notifyItemRemoved(position);
                })
                .deleteBattery(id);
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarCreator.createToolbar(ToolbarEnums.SITE_DETAILS, null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ToolbarCreator.createToolbar(ToolbarEnums.SITE_AVAILABLE, null);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (rxBusAddNewBatterySubscription != null && !rxBusAddNewBatterySubscription.isUnsubscribed())
            rxBusAddNewBatterySubscription.unsubscribe();
    }

    private void fetchBatteries(String siteId) {
        new NetApi((MainActivity) getActivity())
                .setNetworkListener(new NetListener<BatteriesResponseData>() {
                    @Override
                    public void onSuccess(BatteriesResponseData responseData) {
                        storeBatteriesToDB(responseData.getBatteryEntities());
                    }
                }).getBatteries(siteId);
    }

    private void storeBatteriesToDB(List<BatteryEntity> batteryEntities){
        DataBaseApi.getInstance().storeBatteries(batteryEntities)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(batteryEntities1 -> {
                    adapter.setBatteryEntities((List<BatteryEntity>) batteryEntities1);
                });
    }

    @Nullable
    @Override
    public Class<SiteDetailsFragmentVM> getViewModelClass() {
        return SiteDetailsFragmentVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.fragment_site_details, BR.viewModel, getContext());
    }

    @Override
    public void onClickPosition(BatteryEntity batteryEntity) {
        mActivity.addFragment(new BatteryDetailsFragment().setBatteryDetailsModel(true,
                new BatteryDetailsModel(mActivity.getSelectSiteEntity(), batteryEntity)), true);
    }
}
