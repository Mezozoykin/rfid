package ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model;

import android.databinding.Observable;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;


@Entity
public interface Site extends Observable, Parcelable, Persistable {
    @Key
    String getId();
    void setId(String id);


    String getTitle();
    void setTitle(String title);

    String getComment();
    void setComment(String comment);


    String getLat();
    void setLat(String lat);


    String getLon();
    void setLon(String lon);
}


