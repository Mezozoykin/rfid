package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;

/**
 * Created by user on 29.12.2016.
 */

public class AddEditBatteryResponse  extends BaseResponseData {
    @SerializedName("battery")
    private BatteryEntity batteryEntity;

    public BatteryEntity getBatteryEntity() {
        return batteryEntity;
    }

    public void setBatteryEntity(BatteryEntity batteryEntity) {
        this.batteryEntity = batteryEntity;
    }
}