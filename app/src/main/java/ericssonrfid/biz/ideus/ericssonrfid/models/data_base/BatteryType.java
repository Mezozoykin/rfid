package ericssonrfid.biz.ideus.ericssonrfid.models.data_base;

import android.databinding.Observable;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by user on 27.12.2016.
 */
@Entity
public interface BatteryType extends Observable, Parcelable, Persistable {

    @Key
    @Generated
    int getId();

    String getTypeName();
    void setTypeName(String typeName);

    int getBrand();
    void setBrand(int brand);

    int getCapacity();
    void setCapacity(int capacity);

    int getDesignLife();
    void setDesignLife(int designLife);

    int getCycleLife();
    void setCycleLife(int cycleLife);

}
