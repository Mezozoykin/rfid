package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by walkmn on 26.12.16.
 */

public abstract class BaseRequest<T extends BaseRequestParams> {

    public abstract String getCommand();

    public BaseRequest(T params) {
        this.params = params;
        command = getCommand();
    }

    public T getParams() {
        return params;
    }

    @SerializedName("params")
    private T params;

    @SerializedName("command")
    private String command;
}
