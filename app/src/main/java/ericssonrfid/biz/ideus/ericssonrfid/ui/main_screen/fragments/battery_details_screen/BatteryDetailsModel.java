package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.battery_details_screen;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;


public class BatteryDetailsModel {
    private SiteEntity siteEntity;
    private BatteryEntity batteryEntity;

    public BatteryDetailsModel(SiteEntity siteEntity, BatteryEntity batteryEntity){
        this.siteEntity = siteEntity;
        this.batteryEntity = batteryEntity;
    }


    public SiteEntity getSiteEntity() {
        return siteEntity;
    }

    public BatteryEntity getBatteryEntity() {
        return batteryEntity;
    }

}
