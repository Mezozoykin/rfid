package ericssonrfid.biz.ideus.ericssonrfid.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemSiteBinding;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;


public class SiteRecyclerAdapter extends RecyclerView.Adapter<SiteRecyclerAdapter.SiteItemHolder> {


   private List<SiteEntity> siteEntities = new ArrayList<>();
private OnSelectClickListener onSelectClickListener;

    public SiteRecyclerAdapter(OnSelectClickListener onSelectClickListener, List<SiteEntity> siteEntities) {
        this.siteEntities = siteEntities;
        this.onSelectClickListener = onSelectClickListener;
    }

    public void setSiteEntities(List<SiteEntity> siteEntities){
        this.siteEntities = siteEntities;
        notifyDataSetChanged();
    }


    @Override
    public SiteItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SiteItemHolder(DataBindingUtil.inflate(inflater,
                R.layout.item_site, parent, false).getRoot());

    }

    @Override
    public void onBindViewHolder(SiteItemHolder holder, int position) {
        SiteEntity siteEntity = siteEntities.get(position);
       holder.binding.setViewModel(siteEntity);
        holder.binding.selectBtn.setOnClickListener(v -> onSelectClickListener.onClickPosition(siteEntity));
    }


    @Override
    public int getItemCount() {
        return siteEntities.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public static class SiteItemHolder extends RecyclerView.ViewHolder {
        ItemSiteBinding binding;

        public SiteItemHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }

    public interface OnSelectClickListener{
        void onClickPosition(SiteEntity siteEntity);
    }

}

