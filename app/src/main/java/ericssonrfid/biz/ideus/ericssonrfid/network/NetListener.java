package ericssonrfid.biz.ideus.ericssonrfid.network;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.BaseResponseData;

public interface NetListener<T extends BaseResponseData> {
    void onSuccess(T responseData);
}
