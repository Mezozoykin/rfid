package ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar;

import android.view.View;

import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ActivityMainBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;


public class ToolbarCreator {

    private static MainActivity activity;
    private static ActivityMainBinding binding;

    public ToolbarCreator(MainActivity activity){
        ToolbarCreator.activity = activity;
        ToolbarCreator.binding = activity.getBinding();
    }

    public static void createToolbar(ToolbarEnums toolbarEnums, String toolbarName){
        switch (toolbarEnums){
            case BASE_TOOLBAR:
                binding.toolbar.tvScreenName.setText((toolbarName == null) ? "" : toolbarName);
                binding.toolbar.ivBtnToolbarLeft.setVisibility(View.VISIBLE);
                binding.toolbar.ivBtnToolbarRight.setVisibility(View.GONE);
                binding.toolbar.ivBtnToolbarLeft.setOnClickListener(v -> activity.onBackPressed());
                binding.toolbar.ivBtnToolbarLeft.setImageResource(R.drawable.ic_left_arrow);
                break;
            case SITE_DETAILS:
                binding.toolbar.tvScreenName.setText(activity.getString(R.string.site_details));
                binding.toolbar.ivBtnToolbarLeft.setVisibility(View.VISIBLE);
                binding.toolbar.ivBtnToolbarRight.setVisibility(View.VISIBLE);
                binding.toolbar.ivBtnToolbarLeft.setOnClickListener(v -> activity.onBackPressed());
                binding.toolbar.ivBtnToolbarLeft.setImageResource(R.drawable.ic_left_arrow);
                binding.toolbar.ivBtnToolbarRight.setImageResource(R.drawable.ic_info);
                break;
            case SITE_AVAILABLE:
                binding.toolbar.tvScreenName.setText(activity.getString(R.string.sites_available));
                binding.toolbar.ivBtnToolbarLeft.setVisibility(View.GONE);
                binding.toolbar.ivBtnToolbarRight.setVisibility(View.VISIBLE);
                binding.toolbar.ivBtnToolbarRight.setImageResource(R.drawable.ic_user);
                break;
        }
    }
}
