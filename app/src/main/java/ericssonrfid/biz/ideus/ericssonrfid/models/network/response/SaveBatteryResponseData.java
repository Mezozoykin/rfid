package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;


public class SaveBatteryResponseData extends BaseResponseData {
    @SerializedName("battery")
    private BatteryEntity batteryEntity;

    public BatteryEntity getBatteryEntity() {
        return batteryEntity;
    }

    public void setBatteryEntity(BatteryEntity batteryEntity) {
        this.batteryEntity = batteryEntity;
    }
}
