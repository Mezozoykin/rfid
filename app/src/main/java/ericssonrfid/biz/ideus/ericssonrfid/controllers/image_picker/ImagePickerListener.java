package ericssonrfid.biz.ideus.ericssonrfid.controllers.image_picker;

import android.graphics.Bitmap;

public interface ImagePickerListener {
    void onGotImage(Bitmap bitmap);
}
