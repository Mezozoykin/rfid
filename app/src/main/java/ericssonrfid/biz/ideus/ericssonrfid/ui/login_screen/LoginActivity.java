package ericssonrfid.biz.ideus.ericssonrfid.ui.login_screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ActivityLoginBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseActivity;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;


public class LoginActivity extends BaseActivity<IView, LoginActivityVM, ActivityLoginBinding> implements IView {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.LoginTheme);
        setModelView(this);

        if (EricssonRfidApp.getInstance().isAuthorize()) {
            goToSiteScreen();
        }

    }


    @Nullable
    @Override
    public Class<LoginActivityVM> getViewModelClass() {
        return LoginActivityVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.activity_login, BR.viewModel, this);
    }

    private void goToSiteScreen() {
        this.startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }
}
