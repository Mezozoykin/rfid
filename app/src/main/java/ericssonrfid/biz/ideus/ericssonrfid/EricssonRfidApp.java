package ericssonrfid.biz.ideus.ericssonrfid;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orhanobut.hawk.Hawk;

import ericssonrfid.biz.ideus.ericssonrfid.models.Models;
import ericssonrfid.biz.ideus.ericssonrfid.models.data_base.FillBatteryDictionary;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.rx.RxSupport;
import io.requery.rx.SingleEntityStore;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

public class EricssonRfidApp extends Application {

    private static EricssonRfidApp mInstance = null;

    public static DisplayImageOptions imageLoaderDefaultDisplayOptions;

    private SingleEntityStore<Persistable> dataStore;

    public static EricssonRfidApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        // init
        mInstance = this;

        Hawk.init(this).build();

        FillBatteryDictionary.delete();
        FillBatteryDictionary.fill();

        setupUniversalImageLoaderConfig();

    }
    private void setupUniversalImageLoaderConfig(){
        imageLoaderDefaultDisplayOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(imageLoaderDefaultDisplayOptions)
                .build();
        ImageLoader.getInstance().init(config);
    }


    public SingleEntityStore<Persistable> getDB() {
        if (dataStore == null) {
            DatabaseSource source = new DatabaseSource(this, Models.DEFAULT, 4);

            if (BuildConfig.DEBUG) {
                source.setLoggingEnabled(true);
            }

            source.setTableCreationMode(TableCreationMode.DROP_CREATE);
            if (BuildConfig.DEBUG) {
                // use this in development mode to drop and recreate the tables on every upgrade
                source.setTableCreationMode(TableCreationMode.DROP_CREATE);
            }
            Configuration configuration = source.getConfiguration();
            dataStore = RxSupport.toReactiveStore(new EntityDataStore<>(configuration));
        }
        return dataStore;
    }

    public boolean isAuthorize() {
        return Hawk.contains("token");
    }

    public String getAuthToken() {
        return Hawk.get("token");
    }

    public void saveAuthToken(String token) {
        Hawk.put("token", token);
    }
    public void saveUserAccId(String accId) {
        Hawk.put("accId", accId);
    }

    public void clearToken() {
        Hawk.delete("token");
    }
}
