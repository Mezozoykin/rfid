package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by walkmn on 26.12.16.
 */

public class AccountLoginRequest extends BaseRequest<AccountLoginParams> {

    public AccountLoginRequest(String login, String password) {
        super(new AccountLoginParams(login, password));
    }

    @Override
    public String getCommand() {
        return "account_login";
    }
}

class AccountLoginParams extends BaseRequestParams {
    @SerializedName("login")
    String login;

    @SerializedName("password")
    String password;

    @SerializedName("remember")
    boolean remember = true;

    public AccountLoginParams(String login, String password) {
        this.login = login;
        this.password = password;
    }
}