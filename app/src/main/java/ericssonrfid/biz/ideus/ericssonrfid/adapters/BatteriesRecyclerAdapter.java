package ericssonrfid.biz.ideus.ericssonrfid.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.ItemBatteriesBinding;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;


public class BatteriesRecyclerAdapter extends RecyclerView.Adapter<BatteriesRecyclerAdapter.BatteryItemHolder> {


    private List<BatteryEntity> batteryEntities = new ArrayList<>();
    private OnSelectBatteryClickListener onSelectClickListener;

    public BatteriesRecyclerAdapter(OnSelectBatteryClickListener onSelectClickListener, List<BatteryEntity> batteryEntities) {
        this.batteryEntities = batteryEntities;
        this.onSelectClickListener = onSelectClickListener;
    }

    public void setBatteryEntities(List<BatteryEntity> batteryEntities){
        this.batteryEntities = batteryEntities;
        notifyDataSetChanged();
    }

    public List<BatteryEntity> getBatteryEntities() {
        return batteryEntities;
    }

    @Override
    public BatteriesRecyclerAdapter.BatteryItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new BatteriesRecyclerAdapter.BatteryItemHolder(DataBindingUtil.inflate(inflater,
                R.layout.item_batteries, parent, false).getRoot());

    }

    @Override
    public void onBindViewHolder(BatteriesRecyclerAdapter.BatteryItemHolder holder, int position) {
        BatteryEntity batteryEntity = batteryEntities.get(position);
        holder.binding.setViewModel(batteryEntity);
        holder.binding.setNpp(String.valueOf(position+1));
        holder.binding.selectBtn.setOnClickListener(v -> onSelectClickListener.onClickPosition(batteryEntity));
    }


    @Override
    public int getItemCount() {
        return batteryEntities.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public static class BatteryItemHolder extends RecyclerView.ViewHolder {
        public ItemBatteriesBinding binding;

        public BatteryItemHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
    }

    public interface OnSelectBatteryClickListener{
        void onClickPosition(BatteryEntity batteryEntity);
    }

}
