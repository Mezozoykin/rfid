package ericssonrfid.biz.ideus.ericssonrfid.network;

import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import ericssonrfid.biz.ideus.ericssonrfid.EricssonRfidApp;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.AccountLoginRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.AddSiteRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.DeleteBatteryRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.GetBatteriesRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.GetSitesRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.SaveBatteryRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.SubmitDataRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.BaseResponse;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseActivity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NetApi {

    private static final String URL = "http://ericssonrfid.plur.se/msmapi/";
   // private static final String URL = "http://ericssonrfid.project.dev.ideus.biz/";
    //private static final String URL = "http://ericssonrfid.andrew.dev.ideus.biz/";
    private static final String API_VERSION = "";

    private static final String SERVER_URL = URL + API_VERSION;
    private LoaderType loaderType = LoaderType.NO_LOADER;

    private BaseActivity baseActivity;

    private NetListener networkListener;
    private HashMap<String, ServerCatchErrorListener> serverCatchErrorListeners = new HashMap<>();

    public NetApi() {
    }

    public NetApi(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        this.loaderType = LoaderType.DIALOG_LOADER;
    }

    public NetApi(BaseActivity baseActivity, LoaderType loaderType) {
        this.baseActivity = baseActivity;
        this.loaderType = loaderType;
    }

    private Subscription wrapperObs(Observable observable) {

        if (baseActivity != null) {
            baseActivity.showLoader(loaderType, hashCode());
        }

        return observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer() {
                    @Override
                    public void onCompleted() {
                        hideLoader();
                        /*if (networkListener instanceof NetworkFullListener) {
                            ((NetworkFullListener) networkListener).onCompleted();
                        }*/
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideLoader();

                        /*if (networkListener != null && networkListener instanceof NetworkExpListener) {
                            ((NetworkExpListener) networkListener).onError(999, null);
                        }*/

                        if (e instanceof SocketTimeoutException) {
                            if (baseActivity != null) {
                                //showError(ct.getString(R.string.error_server_is_not_response));
                            }
                            return;
                        }

                        showError(e.getMessage());
                    }

                    @Override
                    public void onNext(Object response) {
                        hideLoader();

                        if (response instanceof BaseResponse) {

                            int code = ((BaseResponse) response).getCode();


                            if (code == 200) {
                                //OpenUApp.getInstance().noConnectionError();
                            }

                            if (code == 401) {
                                EricssonRfidApp.getInstance().clearToken();
                                //OpenUApp.getInstance().getPref().saveUserBalance(0);
                                //OpenUApp.getInstance().goToStart(baseActivity);
                                return;
                            }

                            // process network listener, some strange logic
                            if (networkListener != null) {

                                // success, process it ...
                                if (code == 200) {
                                    networkListener.onSuccess(
                                            ((BaseResponse) response).getData()
                                    );
                                }

                                // We received some server error, or errors
                                else {
                                    /*if (networkListener instanceof NetworkExpListener) {

                                        boolean isNeedToProcess = ((NetworkExpListener) networkListener).onError(code,
                                                ((BaseResponse) response).getErrors());

                                        if (isNeedToProcess) {
                                            showsErrors(((BaseResponse) response).getErrors());
                                        }
                                        // If we have activity (:-)) than toaster, toaster, toaster!!! lol
                                    } else {
                                        showsErrors(((BaseResponse) response).getErrors());
                                    }*/
                                    showsErrors(((BaseResponse) response).getErrors());
                                }
                            }

                            // for future
                            // processing catch error listeners
                            /*if (!serverCatchErrorListeners.isEmpty() && code != 200) {
                                for (String currentErrorKey : serverCatchErrorListeners.keySet()) {

                                    ServerCatchErrorListener currentServerCatchErrorListener = serverCatchErrorListeners.get(currentErrorKey);

                                    ArrayList<String> errors = ((BaseResponse) response).getErrors();
                                    for (String error : errors) {
                                        if (error.contentEquals(currentErrorKey)) {
                                            currentServerCatchErrorListener.onCatchError(code, ((BaseResponse) response).getData());
                                        }
                                    }
                                }
                            }*/
                        }
                    }
                });
    }

    private void hideLoader() {
        if (baseActivity != null) {
            baseActivity.hideLoader(loaderType, this.hashCode());
        }
    }

    private void showsErrors(ArrayList<String> errors) {
        // clear serverCatchErrorListeners errors
        for (String error : errors) {

            if (!serverCatchErrorListeners.containsKey(error)) {
                showError(error);
            }
        }
    }

    private void showError(String message) {
        if (baseActivity != null) {
            Log.d("ERROR", message);
            Toast.makeText(baseActivity, message, Toast.LENGTH_SHORT).show();
        }
    }

    public NetApi setNetworkListener(NetListener networkListener) {
        this.networkListener = networkListener;
        return this;
    }


    protected NetApiService getApi() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        httpClient.addInterceptor(chain -> {

            Request original = chain.request();

            Request.Builder request = original.newBuilder();

            if (EricssonRfidApp.getInstance().isAuthorize()) {
                request.header("Cookie", "session=" + EricssonRfidApp.getInstance().getAuthToken());
            }

            request.header("Content-Type", "application/json");
            request.header("X-Requested-With", "XMLHttpRequest");
            //request.header("deviceId", EricssonRfidApp.getInstance().getDeviceId());
            //request.header("locale", Locale.getDefault().getLanguage());

            request.method(original.method(), original.body());

            return chain.proceed(request.build());
        });

        httpClient.readTimeout(40, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .client(httpClient.build())
                .build();

        return retrofit.create(NetApiService.class);
    }


    // api methods below
    public void login(String email, String password) {
        wrapperObs(getApi().login(new AccountLoginRequest(email, password)));
    }

    public void getSites() {
        wrapperObs(getApi().getSites(new GetSitesRequest()));
    }

    public void addSite(String title, String comment, String lat, String lon) {
        wrapperObs(getApi().addSite(new AddSiteRequest(title, comment, lat, lon)));
    }
    public void getBatteries(String siteId) {
        wrapperObs(getApi().getBatteries(new GetBatteriesRequest(siteId)));
    }

    public void saveBattery(String batteryId, boolean isUploadedPhoto, String siteId, String rfidNumber, String manufacturer, String type, String serialNumber
            , String shippingDate, String soldVendor, String chargeDate) {
                wrapperObs(getApi().saveBattery(new SaveBatteryRequest(batteryId, isUploadedPhoto, siteId
                        ,  rfidNumber, manufacturer, type, serialNumber, shippingDate, soldVendor, chargeDate)));
    }

    public void deleteBattery(int id) {
        wrapperObs(getApi().deleteBattery(new DeleteBatteryRequest(id)));
    }


    public void uploadPhoto(File pictureFile) {
        RequestBody requestBodyfile = RequestBody.create(MediaType.parse("image/jpeg"), pictureFile);
        wrapperObs(getApi().uploadPhoto("application/json", "XMLHttpRequest"
                , "batteryPhoto", pictureFile.getName(), requestBodyfile));
    }

    public void submitData(int id) {
        wrapperObs(getApi().submitData(new SubmitDataRequest(id)));

    }
}
