package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

public class AccountLoginResponseData extends BaseResponseData {

    public String getToken() {
        return token;
    }

    public String getAccId() {
        return accId;
    }

    @SerializedName("token")
    String token;

    @SerializedName("accId")
    String accId;


}
