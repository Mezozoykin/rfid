package ericssonrfid.biz.ideus.ericssonrfid.utils;


public class Constants {
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String SUCCESS_ADD_SITE = "SUCCESS_ADD_SITE";
    public static final String SUCCESS_SAVE_BATTERY = "SUCCESS_SAVE_BATTERY";

}
