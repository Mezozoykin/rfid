package ericssonrfid.biz.ideus.ericssonrfid.models.network.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 04.01.2017.
 */

public class SubmitDataRequest  extends BaseRequest<SubmitDataRequestParams> {

    public SubmitDataRequest(int id) {
        super(new SubmitDataRequestParams(id)) ;
    }

    @Override
    public String getCommand() {
        return "submit_data";
    }
}



class SubmitDataRequestParams extends BaseRequestParams {
    @SerializedName("id")
    private int id;

    public SubmitDataRequestParams(int id) {
        this.id = id;
    }
}

