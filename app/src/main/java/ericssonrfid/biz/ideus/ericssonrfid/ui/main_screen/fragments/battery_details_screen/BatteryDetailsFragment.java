package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.battery_details_screen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.controllers.image_picker.ImagePickerExpListener;
import ericssonrfid.biz.ideus.ericssonrfid.controllers.image_picker.ImagePickerForFragment;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.FragmentBatteryDetailsBinding;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.UploadPhotoData;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetApi;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetListener;
import ericssonrfid.biz.ideus.ericssonrfid.rx_buses.RxBusNewDataEvent;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarEnums;
import ericssonrfid.biz.ideus.ericssonrfid.utils.Constants;


public class BatteryDetailsFragment extends BaseFragment<IView, BatteryDetailsVM, FragmentBatteryDetailsBinding> implements IView, ImagePickerExpListener {

    private BatteryDetailsModel batteryDetailsModel;
    private ImagePickerForFragment imagePickerForFragment;
    private boolean isUploadedPhoto;
    private boolean isBatteryEdit;
    public BatteryDetailsFragment setBatteryDetailsModel(boolean isBatteryEdit, BatteryDetailsModel batteryDetailsModel) {
        this.batteryDetailsModel = batteryDetailsModel;
        this.isBatteryEdit = isBatteryEdit;
        return this;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(this);
        imagePickerForFragment = new ImagePickerForFragment(this, this);

        getBinding().setBattery(batteryDetailsModel.getBatteryEntity());
        getBinding().setSite(batteryDetailsModel.getSiteEntity());
        getBinding().btnSave.setOnClickListener(v ->{if(isBatteryEdit){ updateBattery();} else{ saveBattery();}});
        getBinding().btnAddPhoto.setOnClickListener(v -> imagePickerForFragment.pickImage());


    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarCreator.createToolbar(ToolbarEnums.BASE_TOOLBAR, getString(R.string.battery_details));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ToolbarCreator.createToolbar(ToolbarEnums.SITE_DETAILS, null);

    }

    @Nullable
    @Override
    public Class<BatteryDetailsVM> getViewModelClass() {
        return BatteryDetailsVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.fragment_battery_details, BR.viewModel, getContext());
    }


    private void saveBattery() {
        new NetApi(mActivity)
                .setNetworkListener(responseData -> {
                        RxBusNewDataEvent.instanceOf()
                                .setRxBusNewDataEvent(Constants.SUCCESS_SAVE_BATTERY);
                        BatteryDetailsFragment.this.getFragmentManager().popBackStack();
                })
                .saveBattery(null, isUploadedPhoto, batteryDetailsModel.getSiteEntity().getId()
                        , batteryDetailsModel.getBatteryEntity().getRfidNumber()
                        , batteryDetailsModel.getBatteryEntity().getManufacturer()
                        ,batteryDetailsModel.getBatteryEntity().getType()
                        , batteryDetailsModel.getBatteryEntity().getSerialNumber()
                        , batteryDetailsModel.getBatteryEntity().getShippingDate()
                        , batteryDetailsModel.getBatteryEntity().getSoldVendor()
                        , batteryDetailsModel.getBatteryEntity().getChargeDate());
    }

    private void updateBattery() {
        new NetApi(mActivity)
                .setNetworkListener(responseData -> {
                        RxBusNewDataEvent.instanceOf()
                                .setRxBusNewDataEvent(Constants.SUCCESS_SAVE_BATTERY);
                        BatteryDetailsFragment.this.getFragmentManager().popBackStack();
                })
                .saveBattery(batteryDetailsModel.getBatteryEntity().getId()
                        , isUploadedPhoto, batteryDetailsModel.getSiteEntity().getId()
                        , batteryDetailsModel.getBatteryEntity().getRfidNumber()
                        , batteryDetailsModel.getBatteryEntity().getManufacturer()
                        ,batteryDetailsModel.getBatteryEntity().getType()
                        , batteryDetailsModel.getBatteryEntity().getSerialNumber()
                        , batteryDetailsModel.getBatteryEntity().getShippingDate()
                        , batteryDetailsModel.getBatteryEntity().getSoldVendor()
                        , batteryDetailsModel.getBatteryEntity().getChargeDate());
    }


    private void uploadPhoto(File pictureFile) {
        new NetApi(mActivity)
                .setNetworkListener(new NetListener<UploadPhotoData>() {
                    @Override
                    public void onSuccess(UploadPhotoData uploadPhotoData) {
                        isUploadedPhoto = uploadPhotoData.getTmpfile() != null && !uploadPhotoData.getTmpfile().isEmpty();
                    }
                })
                .uploadPhoto(pictureFile);
    }


    @Override
    public void onGotResult(Uri resultPick) {
        File file = FileUtils.getFile(mActivity, resultPick);
        uploadPhoto(file);
    }

    @Override
    public void onGotImage(Bitmap bitmap) {
        getBinding().ivPhoto.setImageBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imagePickerForFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePickerForFragment.onActivityResult(requestCode, resultCode, data);
    }
}
