package ericssonrfid.biz.ideus.ericssonrfid.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class Utils {

    public static final int maxSizePictureForGroup = 600;

    public static String convertImageToStringForServerProfile(Bitmap imageBitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if(imageBitmap != null) {
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 120, 120, false);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        }else{
            return null;
        }
    }

    public static String convertImageToStringForServerGroup(Bitmap imageBitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if(imageBitmap != null) {
            int sh, sw;
            if (imageBitmap.getHeight() > maxSizePictureForGroup || imageBitmap.getWidth() > maxSizePictureForGroup){
                float ratio = Math.min(
                        (float) maxSizePictureForGroup / imageBitmap.getWidth(),
                        (float) maxSizePictureForGroup / imageBitmap.getHeight());
                sh = Math.round(ratio * imageBitmap.getHeight());
                sw = Math.round(ratio * imageBitmap.getWidth());
                imageBitmap = Bitmap.createScaledBitmap(imageBitmap, sw, sh, false);
            }
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        }else{
            return null;
        }
    }

    public static Bitmap decodeUri(Context ct, Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                ct.getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                ct.getContentResolver().openInputStream(selectedImage), null, o2);
    }
}
