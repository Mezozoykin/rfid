package ericssonrfid.biz.ideus.ericssonrfid.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import ericssonrfid.biz.ideus.ericssonrfid.R;


public class DetailCommentDialog {
    private View.OnClickListener onCancelClickListener;
    private Dialog dialog;
    private TextView tvCancel;

    public DetailCommentDialog(Context context) {
        dialog = new Dialog(context, R.style.DialogStyleWithOutAnim);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom_comments);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
    }


    public DetailCommentDialog setCommentText(String messageText) {
        ((TextView) dialog.findViewById(R.id.tvComment)).setText(messageText);
        return this;
    }

    public DetailCommentDialog setOnCancelClickListener(View.OnClickListener onCancelClickListener) {
        this.onCancelClickListener = onCancelClickListener;
        return this;
    }

    private void setListeners() {
        tvCancel.setOnClickListener(onCancelClickListener);
    }

    public void show() {
        setListeners();
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public void hide() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}

