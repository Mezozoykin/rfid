package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;


public class SitesResponseData extends BaseResponseData {
    @SerializedName("sites")
    private List<SiteEntity> siteEntities;

    public List<SiteEntity> getSiteEntities() {
        return siteEntities;
    }

    public void setSiteEntities(List<SiteEntity> siteEntities) {
        this.siteEntities = siteEntities;
    }
}
