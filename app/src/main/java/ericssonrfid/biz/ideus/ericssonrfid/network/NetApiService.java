package ericssonrfid.biz.ideus.ericssonrfid.network;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.AccountLoginRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.AddSiteRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.DeleteBatteryRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.GetBatteriesRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.GetSitesRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.SaveBatteryRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.request.SubmitDataRequest;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.AccountLoginResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.AddSiteResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.BaseResponse;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.BatteriesResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.DeleteBatteryResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.SaveBatteryResponseData;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.SitesResponseData;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.UploadPhotoData;
import okhttp3.RequestBody;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.SubmitDataResponseData;

import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

public interface NetApiService {

    @POST("/msmapi")
    Observable<BaseResponse<AccountLoginResponseData>> login(@Body AccountLoginRequest loginModel);

    @POST("/msmapi")
    Observable<BaseResponse<SitesResponseData>> getSites(@Body GetSitesRequest sitesRequest);

    @POST("/msmapi")
    Observable<BaseResponse<AddSiteResponseData>> addSite(@Body AddSiteRequest addSiteRequest);

    @POST("/msmapi")
    Observable<BaseResponse<BatteriesResponseData>> getBatteries(@Body GetBatteriesRequest getBatteriesRequest);

    @POST("/msmapi")
    Observable<BaseResponse<SaveBatteryResponseData>> saveBattery(@Body SaveBatteryRequest saveBatteryRequest);

    @POST("/msmapi")
    Observable<BaseResponse<DeleteBatteryResponseData>> deleteBattery(@Body DeleteBatteryRequest deleteBatteryRequest);



    @POST("/msmapi/raw_data")
    Observable<BaseResponse<UploadPhotoData>> uploadPhoto(
            @Header("Accept") String accept
            , @Header("X-Requested-With") String requested
            , @Header("X-Active-Upload-Collection") String collectionName
            , @Header("X-Active-Upload-Filename") String fileName, @Body RequestBody body);

    @POST("/msmapi")
    Observable<BaseResponse<SubmitDataResponseData>> submitData(@Body SubmitDataRequest submitDataRequest);
}
