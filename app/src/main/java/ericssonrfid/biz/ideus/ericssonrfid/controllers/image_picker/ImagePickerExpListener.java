package ericssonrfid.biz.ideus.ericssonrfid.controllers.image_picker;

import android.net.Uri;

public interface ImagePickerExpListener extends ImagePickerListener{
    void onGotResult(Uri resultPick);
}