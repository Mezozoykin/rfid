package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.add_site_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.FragmentAddSiteBinding;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarEnums;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;


public class AddSiteFragment extends BaseFragment<IView, AddSiteFragmentVM, FragmentAddSiteBinding>
        implements IView {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(this);


    }

    @Nullable
    @Override
    public Class<AddSiteFragmentVM> getViewModelClass() {
        return AddSiteFragmentVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.fragment_add_site, BR.viewModel, getContext());
    }


    @Override
    public void onResume() {
        super.onResume();
        ToolbarCreator.createToolbar(ToolbarEnums.BASE_TOOLBAR, getString(R.string.add_sites));
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ToolbarCreator.createToolbar(ToolbarEnums.SITE_AVAILABLE, null);

    }

}

