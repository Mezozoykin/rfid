package ericssonrfid.biz.ideus.ericssonrfid.network;

/**
 * Created by walkmn on 27.12.16.
 */

public enum LoaderType {
    NO_LOADER,
    DIALOG_LOADER,
    PROGRESS_BAR_LOADER
}
