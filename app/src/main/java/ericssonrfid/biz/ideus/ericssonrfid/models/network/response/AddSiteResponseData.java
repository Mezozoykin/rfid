package ericssonrfid.biz.ideus.ericssonrfid.models.network.response;

import com.google.gson.annotations.SerializedName;

import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;


public class AddSiteResponseData extends BaseResponseData {
    @SerializedName("site")
    private SiteEntity siteEntity;

    public SiteEntity getSiteEntity() {
        return siteEntity;
    }

    public void setSiteEntity(SiteEntity siteEntity) {
        this.siteEntity = siteEntity;
    }
}