package ericssonrfid.biz.ideus.ericssonrfid.models.data_base;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by user on 27.12.2016.
 */

@Entity
public interface RFIDInfo extends Observable, Parcelable, Persistable {

    @Key
    Integer getId();

    @Bindable
    int getSiteId();
    void setSiteId(int siteId);


    @Bindable
    String getRfidNumber();
    void setRfidNumber(String rfidNumber);

    @Bindable
    String getManufacturer();
    void setManufacturer(String manufacturer);

    @Bindable
    String getType();
    void setType(String type);

    @Bindable
    String getSerialNumber();
    void setSerialNumber(String serialNumber);

    @Bindable
    String getShippingDate();
    void setShippingDate(String shippingDate);

    @Bindable
    String getSoldVendor();
    void setSoldVendor(String soldVendor);

    @Bindable
    String getChargeDate();
    void setChargeDate(String chargeDate);

}
