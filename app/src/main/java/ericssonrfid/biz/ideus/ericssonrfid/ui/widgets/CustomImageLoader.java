package ericssonrfid.biz.ideus.ericssonrfid.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import ericssonrfid.biz.ideus.ericssonrfid.R;


@BindingMethods({
        @BindingMethod(type = CustomImageLoader.class, attribute = "imageUrl",
                method = "loadImage")
        ,
        @BindingMethod(type = CustomImageLoader.class, attribute = "imageFlag",
                method = "setImageFlag")})
public class CustomImageLoader extends ImageView {
    private String url;
    private String imageFlag;

    public void setImageFlag(String imageFlag) {
        this.imageFlag = imageFlag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public CustomImageLoader(Context context) {
        super(context);
    }

    public CustomImageLoader(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs
                , R.styleable.CustomImageLoader, 0, 0);
        loadImage(
                typedArray.getString(R.styleable.CustomImageLoader_imageUrl)
        );
        typedArray.recycle();
    }


    public CustomImageLoader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void loadImage(String imageUrl) {
        if (imageUrl != null && !imageUrl.equals("")) {
            setUrl(imageUrl);
            ImageLoader.getInstance().displayImage(imageUrl, this);
        }
        else {
                setImageResource(R.drawable.ic_photo_camera);
            }
        }
    }


