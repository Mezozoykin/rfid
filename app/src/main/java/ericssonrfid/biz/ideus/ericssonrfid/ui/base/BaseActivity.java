package ericssonrfid.biz.ideus.ericssonrfid.ui.base;

import android.app.AlertDialog;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import biz.ideus.ideuslib.mvvm_lifecycle.AbstractViewModel;
import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.base.ViewModelBaseActivity;
import ericssonrfid.biz.ideus.ericssonrfid.network.LoaderDialog;
import ericssonrfid.biz.ideus.ericssonrfid.network.LoaderType;


public abstract class BaseActivity<T extends IView, R extends AbstractViewModel<T>, B extends ViewDataBinding>
        extends ViewModelBaseActivity<T, R>
        implements IView {

    protected B binding;
    protected R viewModel;

    private HashMap<Integer, LoaderDialog> loaderDialogs = new HashMap<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModeHelper.performBinding(this);
        binding = getBinding();
        viewModel = getViewModel();

    }



    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(this.INPUT_METHOD_SERVICE);
        View view = this.getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @SuppressWarnings("unused")
    @NotNull
    public B getBinding() {
        try {
            return (B) mViewModeHelper.getBinding();
        } catch (ClassCastException ex) {
            throw new IllegalStateException("Method getViewModelBindingConfig() has to return same " +
                    "ViewDataBinding type as it is set to base Fragment");
        }
    }
//    public void addFragment(Fragment fragment, Bundle args, boolean addToBackstack, String backstackTag) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        if (!isAlreadyAddedFragment(fragmentManager, fragment.getClass().getSimpleName())) {
//            if (args != null) {
//                fragment.setArguments(args);
//            }
//            FragmentTransaction ft = fragmentManager.beginTransaction()
//                    .setCustomAnimations(biz.ideus.ideuslib.R.anim.slide_up, biz.ideus.ideuslib.R.anim.slide_down
//                            , biz.ideus.ideuslib.R.anim.slide_up, biz.ideus.ideuslib.R.anim.slide_down)
//                    .add(android.R.id.content, fragment, fragment.getClass().getSimpleName());
//            if (addToBackstack) {
//                ft.addToBackStack(backstackTag).commit();
//                fragmentManager.executePendingTransactions();
//            } else {
//                ft.commit();
//            }
//        }
//    }
//
//    private boolean isAlreadyAddedFragment(FragmentManager fragmentManager, String fragmentTag){
//        return fragmentManager.popBackStackImmediate(fragmentTag , 0) && fragmentManager.findFragmentByTag(fragmentTag) == null;
//    }


    public void createErrorDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_warning)
                .setCancelable(false)
                .setNegativeButton(getString(ericssonrfid.biz.ideus.ericssonrfid.R.string.ok),
                        (dialog, id) -> dialog.cancel());
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void showLoader(LoaderType loaderType, int hashCode) {
        switch (loaderType) {
            case DIALOG_LOADER:
                loaderDialogs.put(hashCode, new LoaderDialog(this));
                loaderDialogs.get(hashCode).show();
                break;


            case PROGRESS_BAR_LOADER:
                //activityBaseBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
                break;

            case NO_LOADER:
                break;

            default:
                break;
        }
    }

    public void hideLoader(LoaderType loaderType, int hash) {

        switch (loaderType) {
            case DIALOG_LOADER:
                if (loaderDialogs.get(hash) != null && loaderDialogs.get(hash).isShowing()) {
                    loaderDialogs.get(hash).dismiss();
                }
                break;

            case PROGRESS_BAR_LOADER:
                //activityBaseBinding.progressBar.progressBar.setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }
}

