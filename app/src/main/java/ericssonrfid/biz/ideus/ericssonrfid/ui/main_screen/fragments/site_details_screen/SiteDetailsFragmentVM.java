package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.site_details_screen;

import android.content.Context;
import android.databinding.ObservableField;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import biz.ideus.ideuslib.mvvm_lifecycle.AbstractViewModel;
import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import ericssonrfid.biz.ideus.ericssonrfid.dialogs.DetailCommentDialog;
import ericssonrfid.biz.ideus.ericssonrfid.dialogs.ScanDialog;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.BatteryEntity;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;
import ericssonrfid.biz.ideus.ericssonrfid.nfc.nfc_models.BaseRecord;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity.MainActivity;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.battery_details_screen.BatteryDetailsFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.battery_details_screen.BatteryDetailsModel;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.summary.SummaryFragment;


public class SiteDetailsFragmentVM extends AbstractViewModel<IView> implements MainActivity.OnNFCReadListener {
    private Context context;
    private SiteEntity siteEntity;

    public static final int COUNT_SYMBOL_TO_TRIM = 100;

    public ObservableField<String> siteName = new ObservableField<>();
    public ObservableField<String> locationTitle = new ObservableField<>();
    public ObservableField<String> commentDetails = new ObservableField<>();
    public ObservableField<Integer> visibilityCommentField = new ObservableField<>();


    @Override
    public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
        super.onCreate(arguments, savedInstanceState);
    }

    @Override
    public void onBindView(@NonNull IView view) {
        super.onBindView(view);
        context = view.getViewModelBindingConfig().getContext();
        siteEntity = ((MainActivity) context).getSelectSiteEntity();
        ((MainActivity) context).setOnNFCReadListener(this);
        showSiteData();
    }

    private void showSiteData() {
        siteName.set(siteEntity.getTitle());
        locationTitle.set(siteEntity.getLat() + " | " + siteEntity.getLon());
        commentDetails.set(isBigCommentText() ? siteEntity.getComment().substring(0, COUNT_SYMBOL_TO_TRIM) + "..." : siteEntity.getComment());
        visibilityCommentField.set(!siteEntity.getComment().isEmpty() ? View.VISIBLE : View.GONE);
    }

    public void onClickCommentDetails(View view) {
        if (isBigCommentText())
            createDialogCommentDetails(siteEntity.getComment());
    }

    private boolean isBigCommentText() {
        return siteEntity.getComment().length() >= COUNT_SYMBOL_TO_TRIM;
    }

    public void createDialogCommentDetails(String comment) {
        DetailCommentDialog detailCommentDialog = new DetailCommentDialog(context);
        detailCommentDialog.setCommentText(comment)
                .setOnCancelClickListener(views -> {
                    detailCommentDialog.hide();
                })
                .show();
    }


    public void onSummaryClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("Site", siteEntity.getId());
        SummaryFragment summaryFragment = new SummaryFragment();
        summaryFragment.setArguments(bundle);
        ((MainActivity) context).addFragment(summaryFragment, true);
    }

    public void onScanClick(View view) {
        ScanDialog.getInstance().show(((MainActivity) context).getFragmentManager(), "dialog");
    }

    @Override
    public void onNfcReaded(BaseRecord baseRecord) {
        Log.d("nfcRec", baseRecord.payload.toString());
        playNotificationSound();

        BatteryEntity testBatteryEntity = createBatteryEntity(getBatteryInfoArray(baseRecord));
        BatteryDetailsModel batteryDetailsModel = new BatteryDetailsModel(siteEntity, testBatteryEntity);
        ((MainActivity) context).addFragment(new BatteryDetailsFragment().setBatteryDetailsModel(false, batteryDetailsModel), true);
    }

    private void playNotificationSound() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String[] getBatteryInfoArray(BaseRecord baseRecord) {
        return baseRecord.payload.split(",");
    }

    private BatteryEntity createBatteryEntity(String[] batteryInfoArray) {
        try {
            return generateTestBatteryEntity(batteryInfoArray);
        } catch (Exception e) {
            return new BatteryEntity();
        }

    }

    @NonNull
    private BatteryEntity generateTestBatteryEntity(String[] batteryInfoArray) throws Exception {
        BatteryEntity testBatteryEntity = new BatteryEntity();
        testBatteryEntity.setId(String.valueOf((int) (Math.random() * 100)));
        testBatteryEntity.setRfidNumber(batteryInfoArray[0]);
        testBatteryEntity.setManufacturer(batteryInfoArray[1]);
        testBatteryEntity.setType(batteryInfoArray[2]);
        testBatteryEntity.setSerialNumber(batteryInfoArray[3]);
        testBatteryEntity.setShippingDate(batteryInfoArray[4]);
        testBatteryEntity.setSoldVendor(batteryInfoArray[5]);
        testBatteryEntity.setChargeDate(batteryInfoArray[6]);
        testBatteryEntity.setRoomId(siteEntity.getId());
        return testBatteryEntity;
    }

}
