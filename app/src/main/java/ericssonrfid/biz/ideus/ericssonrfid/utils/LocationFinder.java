package ericssonrfid.biz.ideus.ericssonrfid.utils;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationFinder implements LocationListener {

    private LocationManager locationManager;
    private FindLocationListener findLocationListener;

    public LocationFinder(Activity activity, FindLocationListener findLocationListener) {
        this.findLocationListener = findLocationListener;
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

    }

    @Override
    public void onLocationChanged(Location location) {
        findLocationListener.onProviderLocationChanged(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    public void fetchLocationData() throws SecurityException {
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            findLocationListener.onProviderLocationDisable();
        } else if (isNetworkEnabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 300, 1, this);
            findLocationListener.onProviderLocationChanged(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 300, 1, this);
            findLocationListener.onProviderLocationChanged(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
        }

    }

    public void removeLocationUpdates() throws SecurityException {
        locationManager.removeUpdates(this);
    }

    public interface FindLocationListener {
        void onProviderLocationChanged(Location location);

        void onProviderLocationDisable();
    }
}
