package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.checklist_screen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import biz.ideus.ideuslib.mvvm_lifecycle.IView;
import biz.ideus.ideuslib.mvvm_lifecycle.binding.ViewModelBindingConfig;
import ericssonrfid.biz.ideus.ericssonrfid.BR;
import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.adapters.CheckListAdapter;
import ericssonrfid.biz.ideus.ericssonrfid.databinding.FragmentChecklistBinding;
import ericssonrfid.biz.ideus.ericssonrfid.network.NetApi;
import ericssonrfid.biz.ideus.ericssonrfid.ui.base.BaseFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.sites.SiteFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarEnums;


public class CheckListFragment extends BaseFragment<IView, CheckListVM, FragmentChecklistBinding>
        implements IView {
    private CheckListAdapter adapter;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(this);
        initCheckList();
        getBinding().btnSubmit.setOnClickListener(v -> onSubmitClick());
    }

    @Nullable
    @Override
    public Class<CheckListVM> getViewModelClass() {
        return CheckListVM.class;
    }

    @Nullable
    @Override
    public ViewModelBindingConfig getViewModelBindingConfig() {
        return new ViewModelBindingConfig(R.layout.fragment_checklist, BR.viewModel, getContext());
    }

    private void initCheckList() {
        adapter = new CheckListAdapter(getContext());
        getBinding().rViewCheckList.setAdapter(adapter);
        getBinding().rViewCheckList.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));

    }


    @Override
    public void onResume() {
        super.onResume();
        ToolbarCreator.createToolbar(ToolbarEnums.BASE_TOOLBAR, getString(R.string.check_list));
       // mActivity.createBaseToolbar(getString(R.string.check_list));
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ToolbarCreator.createToolbar(ToolbarEnums.BASE_TOOLBAR, getString(R.string.summary));
        //mActivity.createBaseToolbar(getString(R.string.summary));

    }

    public void onSubmitClick(){
//        if (!adapter.isAllCheck()) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setTitle("Attention")
//                    .setMessage("Please, check all items")
//                    .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_warning)
//                    .setCancelable(false)
//                    .setPositiveButton(getActivity().getString(R.string.ok),
//                            (dialog, id) -> dialog.dismiss());
//            AlertDialog dialog = builder.create();
//            dialog.show();
//        } else {
            new NetApi(mActivity)
                    .setNetworkListener(responseData -> {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                        builder.setTitle("Success")
                                .setMessage("Your data was successfully submited")
                                .setIcon(ericssonrfid.biz.ideus.ericssonrfid.R.drawable.ic_info)
                                .setCancelable(false)
                                .setPositiveButton(mActivity.getString(R.string.gotosites),
                                        (dialog, id) -> {
                                            dialog.dismiss();
                                            mActivity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            mActivity.addFragment(new SiteFragment(), false);
                                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    })
                    .submitData(Integer.valueOf(mActivity.getSelectSiteEntity().getId()));
      //  }
    }

}
