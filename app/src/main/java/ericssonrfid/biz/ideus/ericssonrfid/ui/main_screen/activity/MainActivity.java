package ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import ericssonrfid.biz.ideus.ericssonrfid.R;
import ericssonrfid.biz.ideus.ericssonrfid.models.network.response.response_entities_model.SiteEntity;
import ericssonrfid.biz.ideus.ericssonrfid.nfc.nfc_models.BaseRecord;
import ericssonrfid.biz.ideus.ericssonrfid.ui.main_screen.fragments.sites.SiteFragment;
import ericssonrfid.biz.ideus.ericssonrfid.ui.toolbar.ToolbarCreator;


public class MainActivity extends NFCActivity {
    private OnRequestPermissionsListener onRequestPermissionsListener;
    private OnNFCReadListener onNFCReadListener;

    private ToolbarCreator toolbarCreator;
    private SiteEntity selectSiteEntity = new SiteEntity();


    public void setOnNFCReadListener(OnNFCReadListener onNFCReadListener) {
        this.onNFCReadListener = onNFCReadListener;
    }

    public SiteEntity getSelectSiteEntity() {
        return selectSiteEntity;
    }

    public void setSelectSiteEntity(SiteEntity selectSiteEntity) {
        this.selectSiteEntity = selectSiteEntity;
    }


    public void setOnRequestPermissionsListener(OnRequestPermissionsListener onRequestPermissionsListener) {
        this.onRequestPermissionsListener = onRequestPermissionsListener;
    }


    @Override
    void setNfcData(BaseRecord result) {
        onNFCReadListener.onNfcReaded(result);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbarCreator = new ToolbarCreator(this);
        addFragment(new SiteFragment(), false);
    }


    public void addFragment(Fragment fragment, boolean addToBackstack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (!isAlreadyAddedFragment(fragmentManager, fragment.getClass().getSimpleName())) {
            FragmentTransaction ft = fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
            if (addToBackstack) {
                ft.addToBackStack(null).commit();
                fragmentManager.executePendingTransactions();
            } else {
                ft.commit();
            }
        }
    }

    private boolean isAlreadyAddedFragment(FragmentManager fragmentManager, String fragmentTag) {
        return fragmentManager.popBackStackImmediate(fragmentTag, 0) && fragmentManager.findFragmentByTag(fragmentTag) == null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (onRequestPermissionsListener != null)
            onRequestPermissionsListener.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public interface OnRequestPermissionsListener {
        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
    }

    public interface OnNFCReadListener {
        void onNfcReaded(BaseRecord baseRecord);
    }


}
